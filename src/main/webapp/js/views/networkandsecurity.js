/**
 * Created by vinjamurik on 5/24/2017.
 */
jQuery(document).ready(function() {
    //Get the Cached AWS Credentials
    var awsCredentials = JSON.parse(localStorage.getItem("SyntasaAwsCredentials"));
    var vpcsObject = getRequestObject(awsCredentials.awsAccessKey, awsCredentials.awsSecretKey, awsCredentials.region);

    callRest("/vpcs/getAllVpcs", vpcsObject, getVpcsSuccessHandler, getVpcsFaultHandler, "POST");

    var existingKeyPairObjects = getRequestObject(awsCredentials.awsAccessKey, awsCredentials.awsSecretKey, awsCredentials.region);

    callRest("/keypair/getAllKeyPairs", existingKeyPairObjects, getAllKeyPairSuccessHandler, getAllKeyPairFaultHandler, "POST");

    $(document).on("change", "input[name='keyPairCheckboxes']", function() {
        $("input[name='keyPairCheckboxes']").not(this).prop('checked', false);
    });

    $("input[name='keyPairCheckboxes']").change(function() {
        $("input[name='keyPairCheckboxes']").not(this).prop('checked', false);
    });

    $("#selectExistingKeyPairSubmitButton").click(function() {
        processExistingKeyPair(awsCredentials);
    });

});

function processExistingKeyPair(awsCredentials) {
    var existingKeyPairName = $("input[name='keyPairCheckboxes']:checked").val();

    var existingKeypairFile = $("#existingKeyFileInput").prop("files")[0];
    var fileReader = new FileReader();
    fileReader.onload = function () {
        var fileDataAsBase64String = this.result;
        fileDataAsBase64String = fileDataAsBase64String.replace("data:;base64,", "");
        fileDataAsBase64String = fileDataAsBase64String.replace("data:application/octet-stream;base64," , "");
        var requestObj = getCacheKeyPairRequestObject(awsCredentials.awsAccessKey, awsCredentials.awsSecretKey, awsCredentials.region, existingKeyPairName, fileDataAsBase64String);
        alert(requestObj);
        callRest("/keypair/cacheExistingKeyPair", requestObj, processExistingKeyPairSuccessHandler, processExistingKeyPairFaultHandler, "POST");
    };

    fileReader.readAsDataURL(existingKeypairFile);
}

function processExistingKeyPairSuccessHandler(success) {

}

function processExistingKeyPairFaultHandler(fault) {

}

function getAllKeyPairSuccessHandler(success) {
    var data = success.data;

    if(data != null) {
        var keyPairs = data.keyPairsResponse;
        for(i = 0; i < keyPairs.length; i++) {
            var keyPair = keyPairs[i];
            $("#selectExistingKeyPairForm").prepend(getSwitchDiv(keyPair.keyPairName, "keyPairCheckboxes"));
        }
    }
}

function getAllKeyPairFaultHandler(fault) {

}

function getVpcsSuccessHandler(success) {
    var data = success.data;

    if(data != null) {
        var vpcs = data.vpcResponse;
        for(i = 0; i < vpcs.length; i++) {
            var vpc = vpcs[i];
            $("#vpcForm").append(getSwitchDiv(vpc.vpcId, "vpcCheckBoxes"));
        }
    }
}

function getVpcsFaultHandler(fault) {


}

function getSwitchDiv(value, checkboxGroup) {
    var retStr = "";
    retStr += '<div class="form-group row">';
    retStr += '<label class="col-md-3 form-control-label">' + value +'</label>';
    retStr += '<div class="col-md-9">';
    retStr += '<label class="switch switch-icon switch-success">';
    retStr += '<input type="checkbox" name="' + checkboxGroup +  '" value="' + value + '" class="switch-input"/>';
    retStr += '<span class="switch-label" data-on="" data-off=""></span>';
    retStr += '<span class="switch-handle"></span>';
    retStr += '</label>';
    retStr += '</div>';
    retStr += '</div>';
    return retStr;
}

function getRequestObject(accessKey, secretKey, region) {
    var retObj = {
        "header" : getSyntasaHeader(accessKey, secretKey, region),
        "sessionId" : Math.floor(Math.random() * 1000000) + 1,
        "action" : {}
    }
    return retObj;
}

function getCacheKeyPairRequestObject(accessKey, secretKey, region, keyPairName, keyPairContents) {
    var retObj = getRequestObject(accessKey, secretKey, region);

    retObj.action = {
        "cacheKeyPair" : {
            "keyPairName" : keyPairName,
            "keyPairContents" : keyPairContents
        }
    };

    return retObj;
}