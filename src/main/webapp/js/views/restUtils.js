/**
 * Created by vinjamurik on 5/24/2017.
 */

var ROOT_APP_URL = "/cws";

function callRest(url, data, callback, errorCallback, type, synch) {
    var async = !synch;
    if (!data) data = {};
    jQuery.ajax(ROOT_APP_URL+url,
        {
            type: 'POST',
            contentType: 'application/json',
            data: data == null ? null : JSON.stringify(data),
            async: async,
            sync: synch,
            cache: false,
            success: function(jsonObj) {
                callback(jsonObj);
            },
            error: function(jqXHR, status, errorMsg) {
                alert("Error making REST call to " + url + ": " + errorMsg)
                if (errorCallback)
                    errorCallback(status, errorMsg);
            }
        }
    );
}

function callRestGet(url, callback, errorCallBack, synch) {
    var async = !synch;
    jQuery.ajax(ROOT_APP_URL + url,
        {
            type: 'GET',
            contentType: 'application/json',
            async: async,
            sync: synch,
            cache: false,
            success: function(jsonObj) {
                callback(jsonObj);
            },
            error: function(jqXHR, status, errorMsg) {
                alert("Error making REST call to " + url + ": " + errorMsg)
                if (errorCallback)
                    errorCallback(status, errorMsg);
            }
        }
    );
}