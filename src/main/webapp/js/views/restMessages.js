/**
 * Created by vinjamurik on 5/24/2017.
 */

var SALT_KEY = "LJOMDNAiO2WnC1A0";
var DEFAULT_USER = "syntasaUser";

function getSyntasaHeader(accessKey, secretKey, region) {
    var retObj = {
        "requestId" : guid(),
        "timestamp" : jQuery.now(),
        "userId" : DEFAULT_USER,
        "awsCredentials" : getSyntasaAwsCredentials(accessKey, secretKey, region)
    }
    return retObj;
}

function getSyntasaAwsCredentials(accessKey, secretKey, region) {
    var retObj = {
        "awsAccessKey" : accessKey,
        "awsSecretKey" : secretKey,
        "region" : region,
        "saltKey" : SALT_KEY
    };
    return retObj;
}

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}