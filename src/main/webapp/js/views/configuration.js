/**
 * Created by vinjamurik on 5/24/2017.
 */
jQuery(document).ready(function() {
    var prevAWsCredentials = localStorage.getItem("SyntasaTestCredentialsResult");

    if(prevAWsCredentials != null) {
        loadPreviousParams(JSON.stringify(prevAWsCredentials));
    }

    $("#configurationFormSubmitButton").click(function(event) {
        configurationFormClickedHandler();
        event.preventDefault();
    });

    callRestGet("/lists/getListOfAwsRegions",(function(success){
        $('#awsRegionSelect').empty();
        $('#awsRegionSelect').append($('<option></option>').val('0').html('Select an AWS Region'))
        $.each(success, function(i, p) {
            $('#awsRegionSelect').append($('<option></option>').val(p).html(p));
        });
    }));
});

function configurationFormClickedHandler() {
    var accessKey = $("#awsAccessKeyTf").val();
    var secretKey = $("#awsSecretKeyTf").val();
    var region = $("#awsRegionSelect option:selected").val();

    var testCredObj = createCredentialsRequestObject(accessKey, secretKey, region);

    localStorage.setItem("SyntasaTestCredentialsRequest", JSON.stringify(testCredObj));
    callRest("/credentials/testCredentials", testCredObj, testCredentialsServiceSuccessHandler, testCredentialsServiceFaultHandler, "POST");
}

function loadPreviousParams(success) {
    if(success == null || success.indexOf("data") == -1) {
        return;
    } else {
        //Need to parse twice, because when we "stringified" the original JSON object it contained double quotes which were escaped.
        //So first parse removes the escape characters, second parse converts it to an JSON object.
        var resultObj = JSON.parse(success);
        var resultObjEscaped = JSON.parse(resultObj);

        //Set the selected items back to what they were before.
        var requestObj = JSON.parse(localStorage.getItem("SyntasaTestCredentialsRequest"));
        console.log(typeof requestObj);
        console.log(requestObj.header.awsCredentials.region);

        $('#awsRegionSelect option[value=' + requestObj.header.awsCredentials.region +']').prop("selected", true);

        testCredentialsServiceSuccessHandler(resultObjEscaped);
    }
}

function testCredentialsServiceSuccessHandler(success) {
    //Update teh UI Cards based on the results.

    var awsCredentials = success.data.awsCredentialsResponse;
    var header = success.header;

    var hasAccessToEmr = awsCredentials["hasAccessToEmr"];
    var hasAccessToEc2 = awsCredentials["hasAccessToEC2"];
    var hasAccessToS3 = awsCredentials["hasAccessToS3"];
    var hasAccessToRedshift = awsCredentials["hasAccessToRedshift"];
    var hasAccessToRds = awsCredentials["hasAccessToRds"];
    var hasAccessToVpc = awsCredentials["hasAccessToVpc"];

    if(hasAccessToEmr) {
        $("span#emrFullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-success");
        $("span#emrFullAccessCard").html("Success");
    } else {
        $("span#emrFullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-danger");
        $("span#emrFullAccessCard").html("Not Allowed");
    }

    if(hasAccessToEc2) {
        $("span#ec2FullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-success");
        $("span#ec2FullAccessCard").html("Success");
    } else {
        $("span#ec2FullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-danger");
        $("span#ec2FullAccessCard").html("Not Allowed");
    }

    if(hasAccessToS3) {
        $("span#s3FullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-success");
        $("span#s3FullAccessCard").html("Success");
    } else {
        $("span#s3FullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-danger");
        $("span#s3FullAccessCard").html("Not Allowed");
    }

    if(hasAccessToRedshift) {
        $("span#redshiftFullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-success");
        $("span#redshiftFullAccessCard").html("Success");
    } else {
        $("span#redshiftFullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-danger");
        $("span#redshiftFullAccessCard").html("Not Allowed");
    }

    if(hasAccessToRds) {
        $("span#rdsFullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-success");
        $("span#rdsFullAccessCard").html("Success");
    } else {
        $("span#rdsFullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-danger");
        $("span#rdsFullAccessCard").html("Not Allowed");
    }

    if(hasAccessToVpc) {
        $("span#vpcFullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-success");
        $("span#vpcFullAccessCard").html("Success");
    } else {
        $("span#vpcFullAccessCard").removeClass("badge-warning").removeClass("badge-danger").removeClass("badge-success").addClass("badge-danger");
        $("span#vpcFullAccessCard").html("Not Allowed");
    }

    if(hasAccessToEmr && hasAccessToEc2 && hasAccessToS3 && hasAccessToRds && hasAccessToRedshift && hasAccessToVpc) {
        //Enable the Network & Security Page
        $("li#networkAndSecurityPage").removeClass("disabled");
        //Store the results in a cookie variable
        var stringifySuccess = JSON.stringify(success);
        localStorage.setItem("SyntasaTestCredentialsResult", stringifySuccess);
    }
}

function removeBadgeClass(index, clazz) {
    var clazzText = clazz.replace(/badge-[a-zA-z]+/, "");
    return clazzText;
}

function testCredentialsServiceFaultHandler(fault) {
    alert(fault);
}

function createCredentialsRequestObject(accessKey, secretKey, region) {
    var retObj = {
        "header" : getSyntasaHeader(accessKey, secretKey, region),
        "sessionId" : Math.floor(Math.random() * 1000000) + 1,
        "action" : {}
    }

    localStorage.setItem("SyntasaAwsCredentials", JSON.stringify(retObj.header.awsCredentials));
    return retObj;
}