/**
 * Created by vinjamurik on 5/31/2017.
 */
jQuery(document).ready(function() {
    callRestGet("/lists/getListOfAmis", getAmiListSuccessHandler, getAmiListFaultHandler);
    callRestGet("/lists/getListOfInstanceTypes", getInstanceTypesSuccessHandler, getInstanceTypesFaultHandler)
});

function getAmiListSuccessHandler(success) {
    var data = success.data;

    var amis = data.amis;

    for(i = 0; i < amis.length; i++) {
        $("#clusterAmi").append(getOption(amis[i].amiVersion, amis[i].amiVersion));
    }
}

function getAmiListFaultHandler(fault) {

}

function getInstanceTypesSuccessHandler(success) {
    var data = success.data;

    var instanceTypes = data.instances;

    for(i = 0; i < instanceTypes.length; i++) {
        $("#driverInstanceType").append(
                getOption(instanceTypes[i].instanceType,
                instanceTypes[i].instanceType
                + "\t\t| "
                + instanceTypes[i].instanceMemory
                + " GB"
        ));
        $("#slaveInstanceType").append(
            getOption(instanceTypes[i].instanceType,
            instanceTypes[i].instanceType
            + "\t\t| "
            + instanceTypes[i].instanceMemory
            + " GB"
        ));
    }
}

function getInstanceTypesFaultHandler(fault) {

}

function getOption(optionName, optionDisplay) {
    var retStr = '<option value="' + optionName;
    retStr += '">' + optionDisplay + '</option>';

    return retStr;
}
