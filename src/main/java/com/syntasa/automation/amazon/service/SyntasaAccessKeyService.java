/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.model.CreateAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.CreateAccessKeyResult;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Lazy
public class SyntasaAccessKeyService {

    @Autowired
    private ConnectionUtils connectionUtils;

    public String[] createUserAccessKey(String username)
            throws SyntasaException {

        String[] serviceUserCredentials = new String[2];

        AmazonIdentityManagement iam = connectionUtils.getIamClient();
        CreateAccessKeyRequest request = new CreateAccessKeyRequest().withUserName(username);
        CreateAccessKeyResult response = iam.createAccessKey(request);

        serviceUserCredentials[0] = response.getAccessKey().getAccessKeyId();
        serviceUserCredentials[1] = response.getAccessKey().getSecretAccessKey();

        log.info("CREATE USER ACCESS KEY REQUEST: AccessKeyID - " + serviceUserCredentials[0]);
        log.info("CREATE USER ACCESS KEY REQUEST: SecretKey   - " + serviceUserCredentials[1]);

        return serviceUserCredentials;
    }
}
