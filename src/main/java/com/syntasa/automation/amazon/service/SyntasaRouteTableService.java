/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;


import com.amazonaws.services.ec2.model.*;
import com.syntasa.automation.amazon.domain.AwsRouteTable;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.Constants;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class SyntasaRouteTableService {

    @Autowired
    private ConnectionUtils connectionUtils;
    @Autowired
    private SyntasaVpcService syntasaVpcService;
    @Autowired
    private SyntasaSubnetService syntasaSubnetService;
    @Autowired
    private SyntasaIgwService syntasaIgwService;

    public Map<String, String> buildRouteTables(String vpcId, String igwId, Map<String, String> subnetMap) throws SyntasaException {
        Map<String, String> routeTableIds = new HashMap<String, String>();
        for(AwsRouteTable routeTable : connectionUtils.getEnv().getRouteTables()) {
            routeTableIds.put(routeTable.getName(), createSyntasaRouteTable(routeTable, vpcId, igwId, subnetMap));
        }
        return routeTableIds;
    }

    public String createSyntasaRouteTable(AwsRouteTable routeTable, String vpcId, String igwId, Map<String, String> subnetMap)
            throws SyntasaException {

        String subnetId = "";
        for(Map.Entry<String, String> entry : subnetMap.entrySet()) {
            if(entry.getKey().equalsIgnoreCase(routeTable.getAssignedSubnetName())) {
                subnetId = entry.getValue();
            }
        }

        CreateRouteTableRequest create_request = new CreateRouteTableRequest().withVpcId(vpcId);
        CreateRouteTableResult create_result = connectionUtils.getEC2Client().createRouteTable(create_request);
        String routeTableId = create_result.getRouteTable().getRouteTableId();

        log.info("CREATE ROUTE TABLE REQUEST: ID - " + routeTableId);

        connectionUtils.sleep(1);

        if(routeTable.getAssignedSubnetName() != null && routeTable.getAssignedSubnetName().length() > 1) {
            associateSyntasaRouteTableWithSubnet(routeTableId, subnetId);
        }

        if(routeTable.isInternetAccess()) {
            createRouteToIgw(routeTableId, igwId, "0.0.0.0/0");
        }

        CreateTagsRequest tagRequest = new CreateTagsRequest()
                .withTags(new Tag().withKey(Constants.NAME).withValue(routeTable.getName()))
                .withResources(routeTableId);

        connectionUtils.getEC2Client().createTags(tagRequest);

        log.info("TAG REQUEST: route table tagged as - " + routeTable.getName());

        return routeTableId;
    }

    public void associateSyntasaRouteTableWithSubnet(String routeTableId, String subnetId) throws SyntasaException {
        AssociateRouteTableRequest request = new AssociateRouteTableRequest()
                .withSubnetId(subnetId)
                .withRouteTableId(routeTableId);
        connectionUtils.getEC2Client().associateRouteTable(request);
        log.info("ASSOCIATE ROUTE TABLE REQUEST: Route Table ("
                + routeTableId + ") Subnet (" + subnetId + ")");
    }

    public void createRouteToIgw(String routeTableId, String gatewayId, String destinationCidrBlock) throws SyntasaException {
        CreateRouteRequest request = new CreateRouteRequest()
                .withRouteTableId(routeTableId)
                .withGatewayId(gatewayId)
                .withDestinationCidrBlock(destinationCidrBlock);
        connectionUtils.getEC2Client().createRoute(request);
        log.info("CREATE ROUTE TO GATEWAY REQUEST: TableID - " + routeTableId + " GatewayID - " + gatewayId);
    }

    public void deleteSyntasaRouteTables() throws SyntasaException {

        DescribeRouteTablesResult result = connectionUtils.getEC2Client().describeRouteTables();
        for(RouteTable rt : result.getRouteTables()) {
            for(Tag tag : rt.getTags()) {
                for(AwsRouteTable table : connectionUtils.getEnv().getRouteTables()) {
                    if(tag.getKey().equals("Name") && tag.getValue().equals(table.getName())) {
                        DeleteRouteTableRequest delete_request = new DeleteRouteTableRequest()
                                .withRouteTableId(rt.getRouteTableId());
                        connectionUtils.getEC2Client().deleteRouteTable(delete_request);
                        log.info("DELETE ROUTE TABLE REQUEST: ID - " + rt.getRouteTableId());
                    }
                }
            }
        }
    }

}
