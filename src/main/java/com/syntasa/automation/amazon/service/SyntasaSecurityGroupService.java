/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.*;
import com.syntasa.automation.amazon.domain.AwsIpPermission;
import com.syntasa.automation.amazon.domain.AwsSecurityGroup;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.Constants;
import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class SyntasaSecurityGroupService {

    @Autowired
    private ConnectionUtils connectionUtils;

    @Autowired
    private SyntasaVpcService syntasaVpcService;

    public Map<String,String> buildSecurityGroup(String vpcId) throws SyntasaException {
        Map<String,String> groupMap = new HashMap<String,String>();
        for(AwsSecurityGroup sg : connectionUtils.getEnv().getSecurityGroups()) {
            String groupId = createSecurityGroup(sg, vpcId);
            attachSecurityGroupRules(groupId, sg.getIpPermissions());
            groupMap.put(sg.getName(), groupId);
        }

        return groupMap;
    }

    public String createSecurityGroup(AwsSecurityGroup securityGroup, String vpcId) throws SyntasaException {
        try {
            AmazonEC2 ec2 = connectionUtils.getEC2Client();
            CreateSecurityGroupRequest csgr = new CreateSecurityGroupRequest()
                    .withGroupName(securityGroup.getName())
                    .withDescription(securityGroup.getDescription())
                    .withVpcId(vpcId);
            CreateSecurityGroupResult response = ec2.createSecurityGroup(csgr);
            log.info("CREATE SECURITY GROUP REQUEST: " + response);

            //Wait for security group to be created by AWS
            connectionUtils.sleep(7);

            CreateTagsRequest tagRequest = new CreateTagsRequest()
                    .withTags(new Tag().withKey(Constants.NAME).withValue(securityGroup.getName()))
                    .withResources(response.getGroupId());

            connectionUtils.getEC2Client().createTags(tagRequest);

            log.info("TAG REQUEST: security group tagged as - " + securityGroup.getName());


            return response.getGroupId();
        } catch(Exception e) {
            e.printStackTrace();
            throw new SyntasaException("Error attempting to create security group.",
                    e.getCause(),
                    ErrorCode.GENERIC);
        }
    }

    public void attachSecurityGroupRules(String groupId, List<AwsIpPermission> ipPermissions) throws SyntasaException {
        IpPermission permission;
        for (int i = 0; i < ipPermissions.size(); i++) {
            permission = createIpPermission(ipPermissions.get(i).getCidr(), ipPermissions.get(i).getProtocol(),
                    ipPermissions.get(i).getPort());
            addRuleToSecurityGroup(groupId, permission);
        }
    }

    public IpPermission createIpPermission(String accessIP, String protocol, int port) throws SyntasaException {
        return new IpPermission()
                .withIpv4Ranges(Arrays.asList(new IpRange().withCidrIp(accessIP)))
                .withIpProtocol(protocol)
                .withFromPort(port)
                .withToPort(port);
    }

    public void addRuleToSecurityGroup(String groupId, IpPermission ipPermission) throws SyntasaException {
        if(!ipPermission.getIpv4Ranges().contains("0.0.0.0/0")) {
            AuthorizeSecurityGroupIngressRequest secGroupIngressRequest = new AuthorizeSecurityGroupIngressRequest()
                    .withGroupId(groupId)
                    .withIpPermissions(ipPermission);
            connectionUtils.getEC2Client().authorizeSecurityGroupIngress(secGroupIngressRequest);
            log.info("INGRESS IP ADDRESS SET: GroupID - " + groupId + " IPv4 - " + ipPermission.withIpv4Ranges());
        } else {
            AuthorizeSecurityGroupEgressRequest request = new AuthorizeSecurityGroupEgressRequest()
                    .withGroupId(groupId)
                    .withIpPermissions(ipPermission);
            connectionUtils.getEC2Client().authorizeSecurityGroupEgress(request);
            log.info("EGRESS IP ADDRESS SET: GroupID - " + groupId + " IPv4 - " + ipPermission.withIpv4Ranges());
        }

    }

    /*
    public String getSecurityGroup() throws SyntasaException {

        DescribeSecurityGroupsRequest request = new DescribeSecurityGroupsRequest();

        DescribeSecurityGroupsResult result = connectionUtils.getEC2Client().describeSecurityGroups(request);

        AwsSecurityGroup appSecurityGroup = connectionUtils.getEnv().getSecurityGroups()
                .stream().filter(x -> x.isAppSecurityGroup()).findFirst().get();

        for(SecurityGroup sg : result.getSecurityGroups()) {
            if(sg.getGroupName().equalsIgnoreCase(appSecurityGroup.getName())) {
                return sg.getGroupId();
            }
        }

        return null;
    }
    */

    /*
    public void deleteSecurityGroup() throws SyntasaException {

        DescribeSecurityGroupsRequest request = new DescribeSecurityGroupsRequest();
        request.withFilters(new Filter().withName("tag:" + TAG_NAME).withValues(TAG_VALUE));

        DescribeSecurityGroupsResult result = connectionUtils.getEC2Client().describeSecurityGroups(request);

        log.info("SECURITY GROUPS TO DELETE: " + result.getSecurityGroups().stream().map(i -> i.getGroupName()).collect(Collectors.joining(",")));

        for(SecurityGroup sg : result.getSecurityGroups()) {
            DeleteSecurityGroupRequest delRequest = new DeleteSecurityGroupRequest()
                    .withGroupId(sg.getGroupId());

            DeleteSecurityGroupResult delResult = connectionUtils.getEC2Client().deleteSecurityGroup(delRequest);

            log.info("DELETE SECURITY GROUP REQUEST: " + sg.getGroupName() + " " + delResult.toString());
        }
    }
    */
}
