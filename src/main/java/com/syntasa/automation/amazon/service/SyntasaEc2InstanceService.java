/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.ec2.model.*;
import com.amazonaws.util.StringUtils;
import com.amazonaws.waiters.Waiter;
import com.amazonaws.waiters.WaiterParameters;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.Constants;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class SyntasaEc2InstanceService {

    private static final String TAG_NAME = "createdby";
    private static final String TAG_VALUE = "syntasa";

    @Autowired
    private ConnectionUtils connectionUtils;
    @Autowired
    private SyntasaSecurityGroupService syntasaSecurityGroupService;
    @Autowired
    private SyntasaInstanceProfileService syntasaInstanceProfileService;
    @Autowired
    private SyntasaStartupScriptService syntasaStartupScriptService;

    public String createSyntasaEc2Instance(Map<String, String> subnetMap, Map<String, String> groupMap) throws SyntasaException {

        String ec2SubnetId = "";
        for(Map.Entry<String, String> entry : subnetMap.entrySet()) {
            if(entry.getKey().equalsIgnoreCase(connectionUtils.getEnv().getAppNode().getAssignedSubnetName())) {
                ec2SubnetId = entry.getValue();
            }
        }

        String ec2SecurityGroupId = "";
        for(Map.Entry<String, String> entry : groupMap.entrySet()) {
            if(entry.getKey().equalsIgnoreCase(connectionUtils.getEnv().getAppNode().getAssignedSecurityGroupName())) {
                ec2SecurityGroupId = entry.getValue();
            }
        }

        RunInstancesRequest runInstancesRequest = new RunInstancesRequest()
                .withImageId(getCentOsAmiId())
                .withInstanceType(connectionUtils.getEnv().getAppNode().getEc2InstanceType())
                .withKeyName(connectionUtils.getEnv().getServiceAccount().getKeyFileName())
                .withMinCount(1)
                .withMaxCount(1)
                .withSubnetId(ec2SubnetId)
                .withSecurityGroupIds(ec2SecurityGroupId)
                .withIamInstanceProfile(syntasaInstanceProfileService.getInstanceProfileSpecification())
                .withUserData(syntasaStartupScriptService.getStartupScript())
                .withBlockDeviceMappings(getDefaultBlockDeviceMapping(100));

        RunInstancesResult response = connectionUtils.getEC2Client().runInstances(runInstancesRequest);

        String instanceId = "";

        for(Instance instance : response.getReservation().getInstances()) {
            instanceId = instance.getInstanceId();
        }

        Waiter<DescribeInstancesRequest> waiter = connectionUtils.getEC2Client().waiters().instanceExists();

        waiter.run(new WaiterParameters<>(new DescribeInstancesRequest().withInstanceIds(instanceId)));

        connectionUtils.sleep(25);

        CreateTagsRequest tagRequest = new CreateTagsRequest()
                .withTags(new Tag().withKey(Constants.NAME).withValue(connectionUtils.getEnv().getAppNode().getName()))
                .withResources(instanceId);

        CreateTagsResult tagsResult = connectionUtils.getEC2Client().createTags(tagRequest);

        associateInstanceWithElasticIp(instanceId);

        log.warn("RUN INSTANCE REQUEST: " + instanceId);

        return instanceId;
    }

    public BlockDeviceMapping getDefaultBlockDeviceMapping(int volSize) {
        BlockDeviceMapping retMapping = new BlockDeviceMapping();
        retMapping.withDeviceName("/dev/sda1");
        retMapping.withEbs(new EbsBlockDevice().withDeleteOnTermination(true).withVolumeSize(volSize).withVolumeType(VolumeType.Gp2));
        return retMapping;
    }

    public String getCentOsAmiId() throws SyntasaException {
        DescribeImagesRequest request = new DescribeImagesRequest().withFilters(
                new Filter().withName("name").withValues("CentOS Linux 7 x86_64 HVM EBS*"),
                new Filter().withName("architecture").withValues("x86_64"),
                new Filter().withName("root-device-type").withValues("ebs")

        );

        DescribeImagesResult result = connectionUtils.getEC2Client().describeImages(request);

        Collections.sort(result.getImages(), (o1, o2) -> o2.getCreationDate().compareTo(o1.getCreationDate()));

        log.info("GET CENTOS AMI ID: AMI ID - "  + result.getImages().get(0).getImageId());

        return result.getImages().get(0).getImageId();
    }

    public void associateInstanceWithElasticIp(String instanceId) {
        DescribeAddressesRequest describeAddressesRequest = new DescribeAddressesRequest();
        describeAddressesRequest.withFilters(new Filter().withName("tag:" + TAG_NAME).withValues(TAG_VALUE));

        DescribeAddressesResult describeAddressesResult = connectionUtils.getEC2Client().describeAddresses(describeAddressesRequest);

        String allocationId = null;
        String allocationIp = null;

        if(describeAddressesResult != null && describeAddressesResult.getAddresses() != null && describeAddressesResult.getAddresses().size() > 0) {
            for(Address address : describeAddressesResult.getAddresses()) {
                if(StringUtils.isNullOrEmpty(address.getAssociationId())) {
                    allocationId = address.getAllocationId();
                    allocationIp = address.getPublicIp();
                }
            }
        } else {
            AllocateAddressRequest allocateAddressRequest = new AllocateAddressRequest();
            allocateAddressRequest.withDomain(DomainType.Vpc);

            AllocateAddressResult allocateAddressResult = connectionUtils.getEC2Client().allocateAddress(allocateAddressRequest);
            allocationId = allocateAddressResult.getAllocationId();
            allocationIp = allocateAddressResult.getPublicIp();
        }

        AssociateAddressRequest associateAddressRequest = new AssociateAddressRequest();
        associateAddressRequest.withInstanceId(instanceId);
        associateAddressRequest.withAllocationId(allocationId);


        AssociateAddressResult associateAddressResult = connectionUtils.getEC2Client().associateAddress(associateAddressRequest);

        log.info("Successfully associated instance: " + instanceId + " with ip: " + allocationIp);
    }
/*

    public void terminateEc2Instance() throws SyntasaException {

        DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest()
                .withFilters(new Filter("tag:Name",
                                Collections.singletonList(connectionUtils.getEnv().getAppNode().getInstanceName())));

        DescribeInstancesResult describeInstancesResult = connectionUtils.getEC2Client().describeInstances(describeInstancesRequest);

        String attachedVolumeId = getAttachedVolumeId();

        for(Reservation reservation : describeInstancesResult.getReservations()) {
            for(Instance instance : reservation.getInstances()) {
                for(Tag tag : instance.getTags()) {
                    if(tag.getKey().equals("Name") &&
                            tag.getValue().equals(connectionUtils.getEnv().getAppNode().getInstanceName())) {
                        dissociateAddressFromInstance(instance.getInstanceId());
                        TerminateInstancesRequest request = new TerminateInstancesRequest()
                                .withInstanceIds(instance.getInstanceId());

                        TerminateInstancesResult result = connectionUtils.getEC2Client().terminateInstances(request);

                        Waiter<DescribeInstancesRequest> waiter = connectionUtils.getEC2Client().waiters().instanceTerminated();

                        waiter.run(new WaiterParameters<>(new DescribeInstancesRequest().withInstanceIds(instance.getInstanceId())));

                        log.info("TERMINATE INSTANCE REQUEST: " + result.toString());

                    }
                }
            }
        }

    }
*/

    private void dissociateAddressFromInstance(String instanceid) {
        if(instanceid != null) {
            DescribeAddressesRequest describeAddressesRequest = new DescribeAddressesRequest();
            describeAddressesRequest.withFilters(new Filter().withName("tag:" + TAG_NAME).withValues(TAG_VALUE));

            DescribeAddressesResult describeAddressesResult = connectionUtils.getEC2Client().describeAddresses(describeAddressesRequest);

            for (Address address : describeAddressesResult.getAddresses()) {
                if (!StringUtils.isNullOrEmpty(address.getInstanceId()) && address.getInstanceId().equalsIgnoreCase(instanceid)) {
                    DisassociateAddressRequest disassociateAddressRequest = new DisassociateAddressRequest();
                    disassociateAddressRequest.withAssociationId(address.getAssociationId());

                    DisassociateAddressResult result = connectionUtils.getEC2Client().disassociateAddress(disassociateAddressRequest);
                    log.info("SUCCESSFULLY DISASSOCIATED INSTANCE: " + instanceid + " FROM IP: " + address.getPublicIp());
                }
            }
        } else {
            log.info("Instance ID was null.  No address to disassociate!");
        }
    }
/*

    public String getInstanceId() throws SyntasaException {
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        DescribeInstancesResult result = connectionUtils.getEC2Client().describeInstances(request);
        for(Reservation reservation : result.getReservations()) {
            for(Instance instance : reservation.getInstances()) {
                for(Tag tag : instance.getTags()) {
                    if(tag.getKey().equals("Name") && tag.getValue().equals(
                            connectionUtils.getEnv().getAppNode().getInstanceName())) {

                        return instance.getInstanceId();

                    }
                }
            }
        }
        return null;
    }
*/
/*

    public String getAttachedVolumeId() throws SyntasaException {

        DescribeVolumesRequest request = new DescribeVolumesRequest();
        DescribeVolumesResult result = connectionUtils.getEC2Client().describeVolumes(request);

        String instanceId = getInstanceId();
        for(Volume volume : result.getVolumes()) {
            for(VolumeAttachment attachment : volume.getAttachments()) {

                if(attachment.getInstanceId().equals(instanceId)) {

                    log.info("!!! FOUND VOLUME ID !!! " + volume.getVolumeId());

                    return volume.getVolumeId();

                }
            }
        }
        return null;
    }
*/

    public void deleteAttachedVolume(String volumeId) {
        DeleteVolumeRequest delRequest = new DeleteVolumeRequest().withVolumeId(volumeId);
        connectionUtils.getEC2Client().deleteVolume(delRequest);

        log.info("DELETE VOLUME REQUEST: " + volumeId);
    }


    public void listVolumeAttachments() {
        DescribeVolumesRequest request = new DescribeVolumesRequest();
        DescribeVolumesResult result = connectionUtils.getEC2Client().describeVolumes();
        System.out.println(result);
    }
}
