/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.amazon.service;

import com.amazonaws.util.Base64;
import com.amazonaws.util.StringUtils;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;


@Component
@Slf4j
@Lazy
public class SyntasaStartupScriptService {

    @Autowired
    private ConnectionUtils connectionUtils;

    //todo: get startup script as string, as base64 encoded string, and as file
    public String getStartupScript() throws SyntasaException {
        if(!StringUtils.isNullOrEmpty(connectionUtils.getEnv().getAppNode().getStartupScript().getUnencoded())) {

            return connectionUtils.getEnv().getAppNode().getStartupScript().getUnencoded();

        } else if(!StringUtils.isNullOrEmpty(connectionUtils.getEnv().getAppNode().getStartupScript().getEncoded())) {

            return readEncodedScriptToString();

        } else if(!StringUtils.isNullOrEmpty(connectionUtils.getEnv().getAppNode().getStartupScript().getFilePath())) {

            return readFileScriptToString();

        } else {

            return "";

        }
    }

    public String readEncodedScriptToString() throws SyntasaException {
        byte[] decoded = Base64.decode(connectionUtils.getEnv().getAppNode().getStartupScript().getEncoded());
        try {
            String script = new String(decoded, StandardCharsets.UTF_8);
            return script;
        } catch (Exception e) {
            throw new SyntasaException("Error attempting to decode base64 String.", ErrorCode.GENERIC);
        }
    }

    public String readFileScriptToString() {

        String content = "";
        try {
            content = new String(Files.readAllBytes(
                    Paths.get(connectionUtils.getEnv().getAppNode().getStartupScript().getFilePath())));
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] encodedBytes = Base64.encode(content.getBytes());

        String encodedString = new String(encodedBytes);

        return encodedString;
    }


}
