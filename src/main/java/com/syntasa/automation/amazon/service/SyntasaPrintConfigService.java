/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.amazon.service;


import com.amazonaws.services.ec2.model.DescribeSubnetsRequest;
import com.amazonaws.services.ec2.model.DescribeSubnetsResult;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Tag;
import com.syntasa.automation.amazon.domain.AwsIpPermission;
import com.syntasa.automation.amazon.domain.AwsSecurityGroup;
import com.syntasa.automation.amazon.domain.AwsSubnet;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class SyntasaPrintConfigService {

    @Autowired
    private ConnectionUtils connectionUtils;

    public void createConfigFile(String fileName) {

        String basePath = System.getenv("CONFIG_FOLDER_PATH");

        if(StringUtils.isNotEmpty(basePath)) {
            basePath = basePath.endsWith("/") ? basePath : basePath + "/";
        }

        PrintWriter writer = null;

        try {

            writer = new PrintWriter(basePath + fileName, "UTF-8");

            writer.println("##### ENVIRONMENT #####");
            writer.println("PROJECT_ID=" + connectionUtils.getEnv().getProjectId());
            writer.println("REGION=" + connectionUtils.getEnv().getCredential().getRegion());
            writer.println();
            writer.println("##### NETWORK #####");
            writer.println("VPC_Name=" + connectionUtils.getEnv().getVpc().getName());
            for(AwsSubnet subnet : connectionUtils.getEnv().getSubnets()) {
                writer.println("Subnet_Name=" + subnet.getName());
            }

            writer.println();
            writer.println("##### STORAGE #####");
            writer.println("BUCKET_NAME=" + connectionUtils.getEnv().getProjectId().toLowerCase());
            for(String s : connectionUtils.getEnv().getBucket().getSyntasaBucketFolders()) {
                writer.println("FOLDER: " + s);
            }

            writer.println();
            writer.println("##### METASTORE #####");
            if(connectionUtils.getEnv().getDatabase() != null) {
                writer.println("METASTORE_INSTANCE_NAME=" + connectionUtils.getEnv().getDatabase().getName());
                writer.println("METASTORE_PORT=" + connectionUtils.getEnv().getDatabase().getPort());
                writer.println("METASTORE_USERNAME=" + connectionUtils.getEnv().getDatabase().getMasterUsername());
                writer.println("METASTORE_PASSWORD=" + connectionUtils.getEnv().getDatabase().getMasterPassword());
            }

            writer.println();
            writer.println("##### SECURITY #####");
            writer.println("INSTANCE_PROFILE_NAME=" + connectionUtils.getEnv().getEc2InstanceProfile().getInstanceProfileName());
            writer.println("ACCESS_KEY_ID=" + connectionUtils.getEnv().getCredential().getAccessKey());
            writer.println("SECRET_ACCESS_KEY=" + connectionUtils.getEnv().getCredential().getSecretKey());
            writer.println("KEY_PAIR_NAME=" + connectionUtils.getEnv().getServiceAccount().getKeyFileName());
            for(AwsSecurityGroup securityGroup : connectionUtils.getEnv().getSecurityGroups()) {
                writer.println("SECURITY_GROUP=" + securityGroup.getName());
            }

            writer.println();
            writer.println("##### RUNTIME #####");
            writer.println("EC2_INSTANCE_NAME=" + connectionUtils.getEnv().getAppNode().getName());
            writer.println("EC2_INSTANCE_TYPE=" + connectionUtils.getEnv().getAppNode().getEc2InstanceType());

            writer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        // ENVIRONMENT


    }

}
