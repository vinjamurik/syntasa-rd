/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.ec2.model.*;
import com.syntasa.automation.amazon.domain.AwsSubnet;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.Constants;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class SyntasaSubnetService {

    @Autowired
    private ConnectionUtils connectionUtils;

    public Map<String,String> buildSubnets(String vpcId) throws SyntasaException {
        Map<String,String> subnetMap = new HashMap<String,String>();
        for(AwsSubnet subnet : connectionUtils.getEnv().getSubnets()) {
            subnetMap.put(subnet.getName(),createSyntasaSubnet(subnet, vpcId));
        }
        return subnetMap;
    }

    public String createSyntasaSubnet(AwsSubnet subnet, String vpcId) throws SyntasaException {

        CreateSubnetRequest createSubnetRequest = new CreateSubnetRequest()
                .withVpcId(vpcId)
                .withCidrBlock(subnet.getCidrBlock());
        CreateSubnetResult createSubnetResult = connectionUtils.getEC2Client().createSubnet(createSubnetRequest);
        String subnetId = createSubnetResult.getSubnet().getSubnetId();

        log.info("CREATE SUBNET REQUEST: ID - " + subnetId + " CIDR Block - " + subnet.getCidrBlock());
        if(subnet.isExternalAccess()) {
            enableSubnetAutoAssignPublicIp(subnetId);
        }

        CreateTagsRequest tagRequest = new CreateTagsRequest()
                .withTags(new Tag().withKey(Constants.NAME).withValue(subnet.getName()))
                .withResources(subnetId);

        connectionUtils.getEC2Client().createTags(tagRequest);

        log.info("TAG REQUEST: subnet tagged as - " + subnet.getName());


        return subnetId;
    }

    public void enableSubnetAutoAssignPublicIp(String subnetId) throws SyntasaException {
        ModifySubnetAttributeRequest request = new ModifySubnetAttributeRequest()
                .withSubnetId(subnetId);
        request.setMapPublicIpOnLaunch(true);
        connectionUtils.getEC2Client().modifySubnetAttribute(request);
        log.info("ENABLE AUTO IP ASSIGNMENT ON SUBNET REQUEST: SubnetID - " + subnetId);
    }

    public void deleteSyntasaSubnets() throws SyntasaException {

        DescribeSubnetsResult result = connectionUtils.getEC2Client().describeSubnets(new DescribeSubnetsRequest());

        for(Subnet subnet : result.getSubnets()) {
            for(Tag tag : subnet.getTags()) {
                for (AwsSubnet synSubnet : connectionUtils.getEnv().getSubnets()) {
                    if(tag.getKey().equals("Name") && tag.getValue().equals(synSubnet.getName())) {
                        DeleteSubnetRequest delete_request = new DeleteSubnetRequest().withSubnetId(subnet.getSubnetId());
                        connectionUtils.getEC2Client().deleteSubnet(delete_request);
                        log.info("DELETE SUBNET REQUEST: ID - " + subnet.getSubnetId());
                    }
                }
            }
        }
    }

}
