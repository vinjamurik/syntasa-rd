/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.*;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Lazy
public class SyntasaKeyPairService {
    @Autowired
    private ConnectionUtils connectionUtils;

    public String createSyntasaKeyPair() throws SyntasaException {
            AmazonEC2 ec2 = connectionUtils.getEC2Client();

            CreateKeyPairRequest createKeyPairRequest = new CreateKeyPairRequest();
            createKeyPairRequest.withKeyName(connectionUtils.getEnv().getServiceAccount().getKeyFileName());
            CreateKeyPairResult createKeyPairResult = ec2.createKeyPair(createKeyPairRequest);
            KeyPair keyPair = createKeyPairResult.getKeyPair();
            String privateKey = keyPair.getKeyMaterial();

            log.info("CREATE KEY PAIR REQUEST: PrivateKey - " + privateKey);
            return privateKey;
    }

    public void deleteKeyPair() throws SyntasaException {

        DescribeKeyPairsResult result = connectionUtils.getEC2Client().describeKeyPairs();
        for(KeyPairInfo keypair : result.getKeyPairs()) {
            if(keypair.getKeyName().equals(connectionUtils.getEnv().getServiceAccount().getKeyFileName())) {
                DeleteKeyPairRequest request = new DeleteKeyPairRequest()
                        .withKeyName(keypair.getKeyName());
                connectionUtils.getEC2Client().deleteKeyPair(request);
                log.info("DELETE KEY PAIR REQUEST: " + keypair.getKeyName());
            }
        }

    }
}
