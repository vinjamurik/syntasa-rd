/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.amazon.service;

import com.amazonaws.services.rds.model.CreateDBInstanceRequest;
import com.amazonaws.services.rds.model.CreateDBSubnetGroupRequest;
import com.syntasa.automation.amazon.domain.AwsDatabase;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class SyntasaDatabaseService {

    @Autowired
    private ConnectionUtils connectionUtils;

    public void buildDatabase() {

    }

    public void createDatabase(AwsDatabase databaseObject, Map<String, String> securityGroupMap,
                               Map<String, String> subnetMap) throws SyntasaException {

        List<String> securityGroupIds = new ArrayList<>();
        for(Map.Entry<String, String> entry : securityGroupMap.entrySet()) {
            securityGroupIds.add(entry.getValue());
        }


            CreateDBInstanceRequest createRequest = new CreateDBInstanceRequest()
                    .withDBInstanceIdentifier("TEST AUTOMATION") // Name of the RDS Instance
                    .withDBInstanceClass("db.t2.small") // Size of the instance
                    .withEngine("MySQL") // Specify MySQL .. etc
                    .withPubliclyAccessible(false)
                    .withMultiAZ(false)
                    .withAllocatedStorage(20)
                    .withStorageEncrypted(true)
                    .withAutoMinorVersionUpgrade(true)
                    .withBackupRetentionPeriod(30)
                    .withMasterUsername("TEST")
                    .withMasterUserPassword("TESTTEST123")
                    .withDBSubnetGroupName(createDBSubnetGroup(subnetMap, connectionUtils.getEnv().getDatabase().getConnectedSubnets()))
                    .withVpcSecurityGroupIds(securityGroupIds);

            connectionUtils.getRdsClient().createDBInstance(createRequest);

            log.info("CREATE DATABASE INSTANCE REQUEST: Instance Name - " + "TEST");
    }

    public String createDBSubnetGroup(Map<String, String> subnetMap, List<String> subnetGroup) throws SyntasaException {

        List<String> dbSubnetGroupSubnetIds = new ArrayList<>();
        for(Map.Entry<String, String> entry : subnetMap.entrySet()) {
            if(subnetGroup.contains(entry.getKey())) {
                dbSubnetGroupSubnetIds.add(entry.getValue());
            }
        }

        String subnetGroupName = "TEST GROUP";
        CreateDBSubnetGroupRequest createRequest = new CreateDBSubnetGroupRequest()
                .withDBSubnetGroupName(subnetGroupName)
                .withDBSubnetGroupDescription("DEVOPS TEST - DO NOT USE FOR PRODUCTION")
                .withSubnetIds(dbSubnetGroupSubnetIds);

            connectionUtils.getRdsClient().createDBSubnetGroup(createRequest);

        return subnetGroupName;
    }
}
