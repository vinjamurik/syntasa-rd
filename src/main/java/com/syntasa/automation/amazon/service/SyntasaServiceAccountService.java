/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.model.*;
import com.amazonaws.util.StringUtils;
import com.syntasa.automation.amazon.domain.AwsCredential;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
@Lazy
public class SyntasaServiceAccountService {
    @Autowired
    private ConnectionUtils connectionUtils;

    public void buildServiceAccount() throws SyntasaException {
        try {
            CreateUserRequest request = new CreateUserRequest().withUserName(connectionUtils.getEnv().getServiceAccount()
                    .getUsername());
            connectionUtils.getIamClient().createUser(request);

            log.info("CREATE USER REQUEST: Name - " + connectionUtils.getEnv().getServiceAccount().getUsername());

            attachPolicies(connectionUtils.getEnv().getServiceAccount().getUsername(), connectionUtils.getEnv()
                    .getServiceAccount().getUserPermissions());

            createServiceAccountKeys(connectionUtils.getEnv().getServiceAccount().getUsername());

            //changeClientCredentials();

        } catch (Exception e) {
            throw new SyntasaException("Unable to build User's Service Account", e.getCause() , ErrorCode.GENERIC);
        }
    }

    public void changeClientCredentials() throws Exception {
        CreateAccessKeyRequest createAccessKeyRequest = new CreateAccessKeyRequest();
        createAccessKeyRequest.setUserName(connectionUtils.getEnv().getServiceAccount().getUsername());

        CreateAccessKeyResult accessKeyResult = connectionUtils.getIamClient().createAccessKey(createAccessKeyRequest);

        AccessKey accessKey = accessKeyResult.getAccessKey();

        if(accessKey != null && !StringUtils.isNullOrEmpty(accessKey.getAccessKeyId()) && !StringUtils.isNullOrEmpty(accessKey.getSecretAccessKey())) {
            AwsCredential awsCredential = new AwsCredential();
            awsCredential.setAccessKey(accessKey.getAccessKeyId());
            awsCredential.setSecretKey(accessKey.getSecretAccessKey());
            connectionUtils.initCredentials(awsCredential);
        }
    }

    public void attachPolicies(String serviceUserName, List<String> arns) throws SyntasaException {
        for(String arn : arns) {
            AmazonIdentityManagement iam = connectionUtils.getIamClient();
            AttachUserPolicyRequest request = new AttachUserPolicyRequest()
                    .withUserName(serviceUserName)
                    .withPolicyArn(arn);
            iam.attachUserPolicy(request);
            boolean isPolicyAttached = false;
            int timeout = 0;
            while(!isPolicyAttached) {
                if(checkForPolicy(serviceUserName, arn) == false) {
                    if(timeout <= 10) {
                        try {
                            timeout++;
                            TimeUnit.SECONDS.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        throw new SyntasaException("Timed out waiting for aws to attach user policy",
                                ErrorCode.GENERIC);
                    }
                } else {
                    isPolicyAttached = true;
                }
            }
            log.info("ATTACH USER POLICY REQUEST: ARN - " + arn);
        }
    }

    private boolean checkForPolicy(String serviceUserName, String arn) throws SyntasaException {
        AmazonIdentityManagement iam = connectionUtils.getIamClient();
        ListAttachedUserPoliciesRequest policiesRequest = new ListAttachedUserPoliciesRequest()
                .withUserName(serviceUserName);
        ListAttachedUserPoliciesResult policiesResult = iam.listAttachedUserPolicies(policiesRequest);
        for (AttachedPolicy ap : policiesResult.getAttachedPolicies()) {
            if(ap.getPolicyArn().equals(arn)) {
                return true;
            }
        }
        return false;
    }

    private void createServiceAccountKeys(String username) throws SyntasaException {
        CreateAccessKeyRequest request = new CreateAccessKeyRequest();
        request.withUserName(username);

        CreateAccessKeyResult result = connectionUtils.getIamClient().createAccessKey(request);
        AccessKey key = result.getAccessKey();

        log.info("CREATED ACCESS KEY FOR USER: " + username + "\nACCESS KEY ID: " + key.getAccessKeyId() + "\nSECRET KEY ID: " + key.getSecretAccessKey());
    }

    public void deleteServiceAccount() throws SyntasaException {
        ListUsersResult result = connectionUtils.getIamClient().listUsers();
        for(User user : result.getUsers()) {

            if(user.getUserName().equals(connectionUtils.getEnv().getServiceAccount().getUsername())) {
                deleteUserAccessKey(user.getUserName());
                detachAllUserPolicies(user.getUserName());
                DeleteUserResult deleteUserResult = connectionUtils.getIamClient().deleteUser(
                        new DeleteUserRequest().withUserName(user.getUserName()));
                log.info("DELETE SERVICE ACCOUTN REQUEST: " + deleteUserResult.toString());
            }
        }
    }

    public void deleteUserAccessKey(String username) throws SyntasaException {
        ListAccessKeysRequest listKeysRequest = new ListAccessKeysRequest();
        listKeysRequest.withUserName(username);

        ListAccessKeysResult listKeysResult = connectionUtils.getIamClient().listAccessKeys(listKeysRequest);

        if(listKeysResult != null && listKeysResult.getAccessKeyMetadata() != null) {
            for(AccessKeyMetadata metadata : listKeysResult.getAccessKeyMetadata()) {
                DeleteAccessKeyRequest request = new DeleteAccessKeyRequest()
                        .withUserName(username)
                        .withAccessKeyId(metadata.getAccessKeyId());

                DeleteAccessKeyResult deleteKeyResult = connectionUtils.getIamClient().deleteAccessKey(request);
                if(deleteKeyResult != null) {
                    log.info("DELETED ACCESS KEY FOR USER : " + username + " WITH ACCKEYID: " + metadata.getAccessKeyId());
                }
            }
        }
    }

    public void detachAllUserPolicies(String username) throws SyntasaException {
        ListAttachedUserPoliciesRequest request = new ListAttachedUserPoliciesRequest()
                .withUserName(username);
        ListAttachedUserPoliciesResult response = connectionUtils.getIamClient()
                .listAttachedUserPolicies(request);
        for (AttachedPolicy ap : response.getAttachedPolicies()) {
            DetachUserPolicyRequest detachRequest = new DetachUserPolicyRequest()
                    .withUserName(username)
                    .withPolicyArn(ap.getPolicyArn());
            connectionUtils.getIamClient().detachUserPolicy(detachRequest);
            log.info("DETACH USER POLICY REQUEST: ARN - " + ap.getPolicyArn());
        }
    }
}
