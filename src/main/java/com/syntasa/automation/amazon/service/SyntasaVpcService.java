/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.ec2.model.*;
import com.amazonaws.util.StringUtils;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.Constants;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Lazy
public class SyntasaVpcService {

    @Autowired
    private ConnectionUtils connectionUtils;

    public String buildVpc() throws SyntasaException {
        String vpcId = createVpc();

        if(connectionUtils.getEnv().getVpc().isHostnameEnabled()) {
            enableDnsHostnamesInVpc(vpcId);
        }

        CreateTagsRequest tagRequest = new CreateTagsRequest()
                .withTags(new Tag().withKey(Constants.NAME).withValue(connectionUtils.getEnv().getVpc().getName()))
                .withResources(vpcId);

        connectionUtils.getEC2Client().createTags(tagRequest);

        log.info("TAG REQUEST: vpc tagged as - " + connectionUtils.getEnv().getVpc().getName());

        return vpcId;
    }

    public String createVpc() throws SyntasaException {

        CreateVpcRequest request = new CreateVpcRequest().withCidrBlock(connectionUtils.getEnv().getVpc().getCidrBlock());

        CreateVpcResult response = connectionUtils.getEC2Client().createVpc(request);

        connectionUtils.sleep(2);

        log.info("CREATE VPC REQUEST: ID - " + response.getVpc().getVpcId());

        return response.getVpc().getVpcId();
    }

    public void enableDnsHostnamesInVpc(String vpcId) throws SyntasaException {
        ModifyVpcAttributeRequest request = new ModifyVpcAttributeRequest().withVpcId(vpcId);
        request.setEnableDnsHostnames(true);
        connectionUtils.getEC2Client().modifyVpcAttribute(request);
        log.info("ENABLE DNS HOST NAMES REQUEST: VPC ID - " + vpcId);
    }

    public void deleteVpc(String vpcId) throws SyntasaException {
        if(!StringUtils.isNullOrEmpty(vpcId)) {
            DeleteVpcRequest request = new DeleteVpcRequest().withVpcId(vpcId);
            DeleteVpcResult result = connectionUtils.getEC2Client().deleteVpc(request);
            log.info("DELETE VPC REQUEST: Deleted VPC Response ::: " + result.toString());
        }
    }

}
