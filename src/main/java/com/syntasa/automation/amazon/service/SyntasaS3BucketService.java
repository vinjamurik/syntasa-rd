/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.model.*;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.FileUtils;
import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Component
@Slf4j
@Lazy
public class SyntasaS3BucketService {

    @Autowired
    private ConnectionUtils connectionUtils;
    @Autowired
    private SyntasaKeyPairService syntasaKeyPairService;

    @Autowired
    private FileUtils fileUtils;


    public void buildSyntasaS3Bucket() throws SyntasaException {

        String bucketName = connectionUtils.getEnv().getProjectId().toLowerCase();

        CreateBucketRequest create_request = new CreateBucketRequest(bucketName);

         connectionUtils.getS3Client().createBucket(create_request);

        log.info("CREATED DEFAULT SYNTASA BUCKET: Bucket Name - " + bucketName);

        createS3BucketFolders(bucketName, connectionUtils.getEnv().getBucket().getSyntasaBucketFolders());

        copySyntasaInstallFilesToS3(bucketName);
    }

    public void createS3BucketFolders(String bucketName, List<String> listDefaultFolders) throws SyntasaException {

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

        for(String folder : listDefaultFolders) {

            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folder + "/", emptyContent,
                    metadata);

            connectionUtils.getS3Client().putObject(putObjectRequest);
            log.info("ADDED FOLDER TO SYNTASA BUCKET: Folder Name - " + folder);
        }
    }

    public void saveStringToFile(String privateKey) throws SyntasaException {

        String bucketName = connectionUtils.getEnv().getProjectId().toLowerCase();
        String keynName = connectionUtils.getEnv().getServiceAccount().getKeyFileName();

        connectionUtils.getS3Client().putObject(bucketName, keynName, privateKey);

        log.info("SAVE FILE TO BUCKET REQUEST: Bucket Name - " + bucketName + " Location - " + keynName);
    }

    public void deleteSyntasaS3Bucket() throws SyntasaException {

        List<Bucket> buckets = connectionUtils.getS3Client().listBuckets();

        for(Bucket bucket : buckets) {

            if(bucket.getName().equals(connectionUtils.getEnv().getProjectId())) {

                ObjectListing object_listing = connectionUtils.getS3Client()
                        .listObjects(connectionUtils.getEnv().getProjectId());

                while (true) {

                    for (Iterator<?> iterator = object_listing.getObjectSummaries().iterator(); iterator.hasNext();) {
                        S3ObjectSummary summary = (S3ObjectSummary)iterator.next();
                        connectionUtils.getS3Client().deleteObject(connectionUtils.getEnv().getProjectId(), summary.getKey());
                    }

                    if (object_listing.isTruncated()) {
                        object_listing = connectionUtils.getS3Client().listNextBatchOfObjects(object_listing);
                    } else {
                        break;
                    }
                }

                DeleteBucketRequest request = new DeleteBucketRequest(bucket.getName());
                connectionUtils.getS3Client().deleteBucket(request);

                log.info("DELETE BUCKET REQUEST: Bucket Name - " + bucket.getName());
            }
        }
    }

    private void copySyntasaInstallFilesToS3(String bucketName) throws SyntasaException {
        List<String> fileName = Arrays.asList("install-syntasa-app.sh,syntasa-data-package-4.0.1.tar.gz".split(","));
        List<File> filesToCopy = fileUtils.getFiles(fileName);

        for(File file : filesToCopy) {
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
                    "syn-installation-files/" + file.getName(), file);

            try {
                connectionUtils.getS3Client().putObject(putObjectRequest);
            } catch (SdkClientException e) {
                throw new SyntasaException("Unable to copy file to s3", e, ErrorCode.GENERIC);
            }

            log.info("Successfully copied file: " + file.getName() + " to s3 bucket: " + bucketName + "/syn-installation-files");
        }
    }

}
