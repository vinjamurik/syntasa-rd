/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.ec2.model.*;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.Constants;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Lazy
public class SyntasaIgwService {

    @Autowired
    private ConnectionUtils connectionUtils;

    public String createSyntasaIgw(String vpcId) throws SyntasaException {
        CreateInternetGatewayRequest create_request = new CreateInternetGatewayRequest();
        CreateInternetGatewayResult create_result = connectionUtils.getEC2Client()
                .createInternetGateway(create_request);

        String igwId = create_result.getInternetGateway().getInternetGatewayId();
        log.info("CREATE INTERNET GATEWAY REQUEST: ID - " + igwId);
        connectionUtils.sleep(2);

        attachSyntasaIgwToVpc(igwId, vpcId);

        CreateTagsRequest tagRequest = new CreateTagsRequest()
                .withTags(new Tag().withKey(Constants.NAME).withValue(connectionUtils.getEnv().getInternetGatewayName()))
                .withResources(igwId);

        connectionUtils.getEC2Client().createTags(tagRequest);

        log.info("TAG REQUEST: igw tagged as - " + connectionUtils.getEnv().getInternetGatewayName());

        return igwId;
    }

    public void attachSyntasaIgwToVpc(String igwId, String vpcId) throws SyntasaException {
        AttachInternetGatewayRequest attach_request = new AttachInternetGatewayRequest()
                .withInternetGatewayId(igwId)
                .withVpcId(vpcId);
        connectionUtils.getEC2Client().attachInternetGateway(attach_request);

        while(!isIgwAttached(igwId, vpcId)) {
            connectionUtils.sleep(1);
        }

        log.info("ATTACH INTERNET GATEWAY REQUEST: Attached IGW (" + igwId + ") To VPC (" + vpcId + ")");
    }

    public boolean isIgwAttached(String igwId, String vpcId) throws SyntasaException {
        DescribeInternetGatewaysRequest request = new DescribeInternetGatewaysRequest().withInternetGatewayIds(igwId);
        DescribeInternetGatewaysResult result = connectionUtils.getEC2Client().describeInternetGateways(request);
        for(InternetGateway gateway : result.getInternetGateways()) {
            if(gateway.getInternetGatewayId().equals(igwId)) {
                for(InternetGatewayAttachment attachment : gateway.getAttachments()) {
                    if(attachment.getVpcId().equals(vpcId)) {
                        return true;
                    }
                }
            }
        }
        log.info("ATTACH INTERNET GATEWAY REQUEST: Waiting for gateway to attach to vpc...");
        return false;
    }

}
