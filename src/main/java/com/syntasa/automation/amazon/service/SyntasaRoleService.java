/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.identitymanagement.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.FileUtils;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Lazy
public class SyntasaRoleService {

    @Autowired
    private FileUtils fileUtils;

    @Autowired
    private ConnectionUtils connectionUtils;
    @Autowired
    private ObjectWriter objectWriter;

    public void createSyntasaRole() {

        boolean roleExists = false;
        ListRolesResult rolesList = connectionUtils.getIamClient().listRoles();
        for(Role role : rolesList.getRoles()) {
            if(role.getRoleName().equals(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceRoleName())) {
                roleExists = true;
            }
        }

        if(!roleExists) {
            try {
                String roleDocumentAsString = objectWriter.writeValueAsString(connectionUtils.getEnv()
                        .getEc2InstanceProfile().getRole());

                CreateRoleRequest request = new CreateRoleRequest()
                        .withRoleName(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceRoleName())
                        .withDescription(connectionUtils.getEnv().getEc2InstanceProfile().getRoleDescription())
                        .withAssumeRolePolicyDocument(roleDocumentAsString);

                connectionUtils.getIamClient().createRole(request);

                log.info("CREATE DEFAULT SYNTASA ROLE REQUEST: Role Name - " + request.getRoleName());

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

    }

    public void createAndAttachPolicyForRole() {

        boolean policyExists = false;
        ListPoliciesResult policyList = connectionUtils.getIamClient().listPolicies();
        for(Policy policy : policyList.getPolicies()) {
            if(policy.getPolicyName().equals(connectionUtils.getEnv().getEc2InstanceProfile().getInstancePolicyName())) {
                policyExists = true;
            }
        }

        if(!policyExists) {
            try {

                String PolicyDocumentAsString = objectWriter.writeValueAsString(connectionUtils.getEnv()
                        .getEc2InstanceProfile().getPolicy());

                CreatePolicyRequest create_request = new CreatePolicyRequest()
                        .withPolicyName(connectionUtils.getEnv().getEc2InstanceProfile().getInstancePolicyName())
                        .withPolicyDocument(PolicyDocumentAsString);
                CreatePolicyResult result = connectionUtils.getIamClient().createPolicy(create_request);

                log.info("CREATED POLICY: Name - " + create_request.getPolicyName());

                AttachRolePolicyRequest attachPolicy = new AttachRolePolicyRequest()
                        .withRoleName(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceRoleName())
                        .withPolicyArn(result.getPolicy().getArn());

                connectionUtils.getIamClient().attachRolePolicy(attachPolicy);

                log.info("ATTACHED POLICY TO ROLE: Policy Name - " + create_request.getPolicyName()
                        + " Role Name - " + attachPolicy.getRoleName());

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    public void detachPolicyFromRole() throws SyntasaException {

        ListPoliciesResult listPoliciesResult = connectionUtils.getIamClient().listPolicies(new ListPoliciesRequest());

        String arn = null;
        for(Policy policy : listPoliciesResult.getPolicies()) {
            if(policy.getPolicyName().equals(connectionUtils.getEnv().getEc2InstanceProfile().getInstancePolicyName())) {
                DetachRolePolicyRequest request = new DetachRolePolicyRequest()
                        .withPolicyArn(policy.getArn())
                        .withRoleName(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceRoleName());

                DetachRolePolicyResult result = connectionUtils.getIamClient().detachRolePolicy(request);

                deletePolicy();

                log.info("DETACH POLICY REQUEST: " + result.toString());
            }
        }
    }

    public void deleteRole() throws SyntasaException {
        DeleteRoleRequest request = new DeleteRoleRequest().withRoleName(
                connectionUtils.getEnv().getEc2InstanceProfile().getInstanceRoleName());

        DeleteRoleResult result = connectionUtils.getIamClient().deleteRole(request);

        log.info("DELETE ROLE REQUEST: " + result.toString());
    }

    public void deletePolicy() throws SyntasaException {
        ListPoliciesResult listPoliciesResult = connectionUtils.getIamClient().listPolicies(new ListPoliciesRequest());

        String arn = null;
        for(Policy policy : listPoliciesResult.getPolicies()) {

            if(policy.getPolicyName().equals(connectionUtils.getEnv().getEc2InstanceProfile().getInstancePolicyName())) {

                DeletePolicyRequest request = new DeletePolicyRequest().withPolicyArn(policy.getArn());
                DeletePolicyResult result = connectionUtils.getIamClient().deletePolicy(request);

                log.info("DELETE POLICY REQUEST: " + result.toString());

            }

        }

    }
}


















