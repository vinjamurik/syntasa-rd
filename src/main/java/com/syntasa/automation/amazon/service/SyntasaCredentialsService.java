/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.amazon.service;

import com.amazonaws.services.identitymanagement.model.*;
import com.syntasa.automation.amazon.domain.AwsCredential;
import com.syntasa.automation.amazon.domain.AwsCredentialServicePolicy;
import com.syntasa.automation.amazon.domain.AwsCredentialServiceResponse;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.FileUtils;
import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
@Lazy
public class SyntasaCredentialsService {
    @Autowired
    private ConnectionUtils connectionUtils;
    @Autowired
    private FileUtils fileUtils;

    private List<String> requiredPolicies;

    private static final String ADMINISTRATOR_ACCESS_POLICY_NAME = "AdministratorAccess";

    public AwsCredentialServiceResponse validateCredentials(AwsCredential credential) throws SyntasaException {

        AwsCredentialServiceResponse response = new AwsCredentialServiceResponse();

        response.setCredentialServicePolicies(new ArrayList<>());

        connectionUtils.initCredentials(credential);
        connectionUtils.initRegion(credential.getRegion());

        ListUserPoliciesResult userPoliciesResult = null;
        ListAttachedUserPoliciesResult  attachedUserPoliciesResult = null;
        ListGroupsForUserResult groupsForUserResult = null;

        List<String> allUserPolicyNames = new ArrayList<>();

        try {
            userPoliciesResult = connectionUtils.getIamClient().listUserPolicies(new ListUserPoliciesRequest().withUserName(credential.getUsername()));
            attachedUserPoliciesResult = connectionUtils.getIamClient().listAttachedUserPolicies(new ListAttachedUserPoliciesRequest().withUserName(credential.getUsername()));
            groupsForUserResult = connectionUtils.getIamClient().listGroupsForUser(new ListGroupsForUserRequest().withUserName(credential.getUsername()));

            if(userPoliciesResult != null && userPoliciesResult.getPolicyNames().size() > 0) {
                allUserPolicyNames.addAll(userPoliciesResult.getPolicyNames());
            }

            if(attachedUserPoliciesResult != null && attachedUserPoliciesResult.getAttachedPolicies().size() > 0) {
                for(AttachedPolicy attachedPolicy : attachedUserPoliciesResult.getAttachedPolicies()) {
                    allUserPolicyNames.add(attachedPolicy.getPolicyName());
                }
            }

            if(groupsForUserResult != null && groupsForUserResult.getGroups().size() > 0) {
                for(Group group : groupsForUserResult.getGroups()) {
                    ListGroupPoliciesResult tempResult = connectionUtils.getIamClient().listGroupPolicies(new ListGroupPoliciesRequest().withGroupName(group.getGroupName()));
                    ListAttachedGroupPoliciesResult tempResult1 = connectionUtils.getIamClient().listAttachedGroupPolicies(new ListAttachedGroupPoliciesRequest().withGroupName(group.getGroupName()));

                    if(tempResult != null && tempResult.getPolicyNames() != null) {
                        allUserPolicyNames.addAll(tempResult.getPolicyNames());
                    }

                    if(tempResult1 != null && tempResult1.getAttachedPolicies() != null) {
                        for(AttachedPolicy ap : tempResult1.getAttachedPolicies()) {
                            allUserPolicyNames.add(ap.getPolicyName());
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new SyntasaException("Unable to get Administrator's policies.  Please contact the system administrator", ErrorCode.GENERIC);
        }

        //Check if the user's policy contains an 'Administrator-Access' policy.  If it's there, we're set, no need to check anything.
        boolean isAdmin = false;

        for(String policyName : allUserPolicyNames) {
            if(policyName.toLowerCase().contains(ADMINISTRATOR_ACCESS_POLICY_NAME.toLowerCase())) {
                isAdmin = true;
                break;
            }
        }

        //Iterate through all the requirepolicies and check off the one's we have.  If we have Admin policy, then just set them all to true and return.
        for(String policy : requiredPolicies) {
            AwsCredentialServicePolicy temp = new AwsCredentialServicePolicy();
            temp.setPolicyName(policy);
            temp.setAvailable(isAdmin || allUserPolicyNames.contains(policy));
            response.getCredentialServicePolicies().add(temp);
        }

        //If User didn't have the required policies, then iterate through and then set an error message.
        for(AwsCredentialServicePolicy policy : response.getCredentialServicePolicies()) {
            if(!policy.isAvailable()) {
                response.setError(true);
                response.setMessage("AwsCredentialServicePolicy Validation Failed: Please ensure all policies are attached");
                break;
            }
        }

        log.info("VALIDATE USER CREDENTIAL REQUEST: ERROR=" + response.isError());

        return response;
    }

    @PostConstruct
    private void getRequiredPolicies() {
        this.requiredPolicies = new ArrayList<String>(Arrays.asList(fileUtils.loadRequiredPolicies().split(",")));
    }
}
