/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.service;

import com.amazonaws.services.ec2.model.IamInstanceProfileSpecification;
import com.amazonaws.services.identitymanagement.model.*;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Lazy
public class SyntasaInstanceProfileService {

    @Autowired
    private ConnectionUtils connectionUtils;
    @Autowired
    private SyntasaRoleService syntasaRoleService;

    public String buildSyntasaInstanceProfile() throws SyntasaException {

        ListInstanceProfilesRequest request = new ListInstanceProfilesRequest();
        ListInstanceProfilesResult result = connectionUtils.getIamClient().listInstanceProfiles(request);

        boolean instanceProfileExists = false;
        String instanceProfileId = "";
        for(InstanceProfile ip : result.getInstanceProfiles()) {
            if(ip.getInstanceProfileName().equals(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceProfileName())) {
                instanceProfileExists = true;
                instanceProfileId = ip.getInstanceProfileId();
            }
        }

        if(!instanceProfileExists) {
            syntasaRoleService.createSyntasaRole();
            syntasaRoleService.createAndAttachPolicyForRole();
            instanceProfileId = createSyntasaInstanceProfile();
            attachRoleToInstanceProfile(instanceProfileId);
        }
        return instanceProfileId;
    }

    public String createSyntasaInstanceProfile() throws SyntasaException {

        CreateInstanceProfileRequest req = new CreateInstanceProfileRequest()
                .withInstanceProfileName(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceProfileName());

        CreateInstanceProfileResult res = connectionUtils.getIamClient()
                .createInstanceProfile(req);

        String instanceProfileId = res.getInstanceProfile().getInstanceProfileId();

        log.info("CREATE INSTANCE PROFILE REQUEST: ID - " + instanceProfileId);
        return instanceProfileId;
    }

    public void attachRoleToInstanceProfile(String instanceProfileName) throws SyntasaException {

        AddRoleToInstanceProfileRequest addRequest = new AddRoleToInstanceProfileRequest()
                .withInstanceProfileName(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceProfileName())
                .withRoleName(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceRoleName());

        connectionUtils.getIamClient().addRoleToInstanceProfile(addRequest);

        connectionUtils.sleep(10);

        log.info("ATTACH ROLE TO INSTANCE PROFILE REQUEST: Instance Profile - " + instanceProfileName
                + " Role - " + addRequest.getRoleName());
    }

    public IamInstanceProfileSpecification getInstanceProfileSpecification() throws SyntasaException {
        return new IamInstanceProfileSpecification().withName(
                connectionUtils.getEnv().getEc2InstanceProfile().getInstanceProfileName()
        );
    }

    public void deleteInstanceProfile() throws SyntasaException {
        ListInstanceProfilesResult result = connectionUtils.getIamClient().listInstanceProfiles();
        for(InstanceProfile profile : result.getInstanceProfiles()) {
            if(profile.getInstanceProfileName().equals(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceProfileName())) {


                detachRoleFromInstanceProfile();
                syntasaRoleService.detachPolicyFromRole();
                syntasaRoleService.deleteRole();

                DeleteInstanceProfileResult deleteProfileResult = connectionUtils.getIamClient().deleteInstanceProfile(
                        new DeleteInstanceProfileRequest().withInstanceProfileName(
                                connectionUtils.getEnv().getEc2InstanceProfile().getInstanceProfileName())
                );

                log.info("DELETE INSTANCE PROFILE REQUEST: " + deleteProfileResult.toString());
            }
        }
    }

    public void detachRoleFromInstanceProfile() throws SyntasaException {
        RemoveRoleFromInstanceProfileRequest request = new RemoveRoleFromInstanceProfileRequest()
                .withInstanceProfileName(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceProfileName())
                .withRoleName(connectionUtils.getEnv().getEc2InstanceProfile().getInstanceRoleName());

        RemoveRoleFromInstanceProfileResult result = connectionUtils.getIamClient().removeRoleFromInstanceProfile(request);
        log.info("DETACH ROLE FROM INSTANCE PROFILE REQUEST: " + result.toString());
    }


}
