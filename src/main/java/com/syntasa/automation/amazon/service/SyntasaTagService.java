/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.amazon.service;

import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.Tag;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class SyntasaTagService {

    @Autowired
    private ConnectionUtils connectionUtils;

    public void tagAllAssets(Map<String, String> assets) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy HH-mm-ss");
        LocalDateTime now = LocalDateTime.now();

        for(Map.Entry<String, String> entry : assets.entrySet()) {
            try {
                CreateTagsRequest createTagsRequest = new CreateTagsRequest();
                createTagsRequest.withTags(new Tag().withKey(Constants.CREATEDBY).withValue(connectionUtils.getEnv().getCreatedBy()));
                createTagsRequest.withResources(entry.getValue());
                connectionUtils.getEC2Client().createTags(createTagsRequest);

                CreateTagsRequest createTagsRequest2 = new CreateTagsRequest();
                createTagsRequest2.withTags(new Tag().withKey(Constants.DATE_CREATED).withValue(dtf.format(now)));
                createTagsRequest2.withResources(entry.getValue());
                connectionUtils.getEC2Client().createTags(createTagsRequest2);

            } catch (Exception e) {
                log.warn("Failed to tag asset: Asset Key: " + entry.getKey() + "Asset Value: " + entry.getValue());
            }

        }


        log.info("CREATE TAG REQUEST ::: Tagged all SYNTASA created assets");
    }
}
