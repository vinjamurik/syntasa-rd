/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.syntasa.automation.amazon.domain.SyntasaAwsEnvironment;
import com.syntasa.automation.amazon.service.*;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.Constants;
import com.syntasa.automation.amazon.utils.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class SyntasaAwsTest {

    @Value("classpath:")
    private Resource awsApplicationJson;

    @Autowired
    private ConnectionUtils connectionUtils;
    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    private SyntasaVpcService syntasaVpcService;
    @Autowired
    private SyntasaIgwService syntasaIgwService;
    @Autowired
    private SyntasaSubnetService syntasaSubnetService;
    @Autowired
    private SyntasaRouteTableService syntasaRouteTableService;
    @Autowired
    private SyntasaServiceAccountService syntasaServiceAccountService;
    @Autowired
    private SyntasaAccessKeyService syntasaAccessKeyService;
    @Autowired
    private SyntasaKeyPairService syntasaKeyPairService;
    @Autowired
    private SyntasaRoleService syntasaRoleService;
    @Autowired
    private SyntasaInstanceProfileService syntasaInstanceProfileService;
    @Autowired
    private SyntasaSecurityGroupService syntasaSecurityGroupService;
    @Autowired
    private SyntasaS3BucketService syntasaS3BucketService;
    @Autowired
    private SyntasaEc2InstanceService syntasaEc2InstanceService;
    @Autowired
    private SyntasaTagService syntasaTagService;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private FileUtils fileUtils;

    public void create(String jsonPath) throws Exception {
        SyntasaAwsEnvironment env = null;

        try {
            env = objectMapper.readValue(fileUtils.getFileAsString(FileUtils.AWS_JSON), SyntasaAwsEnvironment.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Maintain list of all syntasa created assets. Used to tag all assets once build is complete.
        Map<String, String> assets = new HashMap<String, String>();

        // Read json config file
        connectionUtils.loadEnvironment(env);

        // Begin installation
        String vpcId = syntasaVpcService.buildVpc();
        assets.put(Constants.VPC, vpcId);

        String igwId = syntasaIgwService.createSyntasaIgw(vpcId);
        assets.put(Constants.IGW, igwId);

        Map<String,String> subnetMap = syntasaSubnetService.buildSubnets(vpcId);
        for(Map.Entry<String, String> entry : subnetMap.entrySet()) {
            assets.put(entry.getKey(), entry.getValue());
        }

        connectionUtils.sleep(2);

        Map<String,String> routTableMap = syntasaRouteTableService.buildRouteTables(vpcId, igwId, subnetMap);
        for(Map.Entry<String, String> entry : routTableMap.entrySet()) {
            assets.put(entry.getKey(), entry.getValue());
        }

        syntasaServiceAccountService.buildServiceAccount();
        String privateKey = syntasaKeyPairService.createSyntasaKeyPair();

        syntasaS3BucketService.buildSyntasaS3Bucket();
        syntasaS3BucketService.saveStringToFile(privateKey);
        syntasaInstanceProfileService.buildSyntasaInstanceProfile();

        Map<String, String> securityGroupMap = syntasaSecurityGroupService.buildSecurityGroup(vpcId);
        for(Map.Entry<String, String> entry : securityGroupMap.entrySet()) {
            assets.put(entry.getKey(), entry.getValue());
        }

        String ec2InstanceId = syntasaEc2InstanceService.createSyntasaEc2Instance(subnetMap, securityGroupMap);
        assets.put(Constants.EC2INSTANCE, ec2InstanceId);

        System.out.println("### ASSETS IN MAP ###");
        for (Map.Entry<String, String> entry : assets.entrySet()) {
            System.out.println("ITEM: " + entry.getKey() + ", Value: " + entry.getValue());
        }

        syntasaTagService.tagAllAssets(assets);

    }

    public void delete(String projectName) throws Exception {

    }
}
