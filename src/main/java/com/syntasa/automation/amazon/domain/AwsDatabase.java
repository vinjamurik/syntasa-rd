/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.amazon.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AwsDatabase {
    private String name;
    private String instanceType;
    private String engine;
    private int port;
    private boolean publiclyAvailable;
    private boolean multiAZ;
    private int allocatedStorage;
    private boolean storageEncryption;
    private boolean autoMinorVersionUpdate;
    private int backupRetentionPeriod;
    private String masterUsername;
    private String masterPassword;
    private List<String> connectedSubnets;
    private List<String> vpcSecuirtyGroups;
}
