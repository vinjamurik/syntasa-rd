/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SyntasaAwsEnvironment {
     private String projectId;
     private String createdBy;
     private AwsCredential credential;
     private AwsVpc vpc;
     private String internetGatewayName;
     private List<AwsSubnet> subnets;
     private List<AwsRouteTable> routeTables;
     private AwsServiceAccount serviceAccount;
     private AwsS3Bucket bucket;
     private AwsInstanceProfile ec2InstanceProfile;
     private List<AwsSecurityGroup> securityGroups;
     private AwsEc2Instance appNode;
     private AwsDatabase database;
}
