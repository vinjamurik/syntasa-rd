/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.utils;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.AmazonRDSClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.syntasa.automation.amazon.domain.AwsCredential;
import com.syntasa.automation.amazon.domain.SyntasaAwsEnvironment;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
@Lazy
@Getter
public class ConnectionUtils {

    private AmazonIdentityManagement iamClient;
    private AmazonEC2 ec2Client;
    private AmazonS3 s3Client;
    private AmazonRDS rdsClient;
    private SyntasaAwsEnvironment env;
    private BasicAWSCredentials credentials;
    private String region;

    public void loadEnvironment(SyntasaAwsEnvironment env) {
        this.env = env;
        log.info("SET AWS ENVIRONMENT");
        initCredentials(env.getCredential());
        initRegion(env.getCredential().getRegion());
    }

    public void initCredentials(AwsCredential credential) {
        this.credentials = new BasicAWSCredentials(credential.getAccessKey(),credential.getSecretKey());
        log.info("INITIALIZED CREDENTIALS ::: Account - " + credential.getUsername());
    }

    public void initRegion(String region) {
        this.region = region;
        log.info("INITIALIZED REGION ::: Region - " + region);
    }

    public AmazonEC2 getEC2Client() {
        if(ec2Client == null) {
            ec2Client = AmazonEC2ClientBuilder.standard()
                    .withRegion(region)
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .build();
        }
        return ec2Client;
    }

    public AmazonIdentityManagement getIamClient() {
        if(iamClient == null) {
            iamClient = AmazonIdentityManagementClientBuilder.standard()
                    .withRegion(region)
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .build();
        }
        return iamClient;
    }

    public AmazonS3 getS3Client() {
        if(s3Client == null) {
            s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .withRegion(region)
                    .build();
        }
        return s3Client;
    }

    public AmazonRDS getRdsClient() {
        if(rdsClient == null) {
            rdsClient = AmazonRDSClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .withRegion(region)
                    .build();
        }
        return rdsClient;
    }

    public void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
