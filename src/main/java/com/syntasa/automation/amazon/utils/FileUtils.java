/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.amazon.utils;

import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
@Lazy
public class FileUtils {

    public static final String AWS_JSON = "aws-application.json";

    @Autowired
    private ResourceLoader resourceLoader;

/*
    public Resource getJsonResource(String filename) throws SyntasaException {
        String basePath = System.getenv("CONFIG_FOLDER_PATH");

        if(StringUtils.isNotEmpty(basePath)) {
            basePath = basePath.endsWith("/") ? basePath : basePath + "/";
            System.out.println("!!!!!! TEST TEST TEST !!!!!! " + basePath + filename);
            return resourceLoader.getResource(basePath + filename);
        }

        throw new SyntasaException("Unable to find file: " + filename + " from basePath: " + basePath, ErrorCode.GENERIC);
    }

    public String getJsonResourceAsString(String fileName) throws SyntasaException {
        try {
            return IOUtils.toString(getJsonResource(fileName).getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            throw new SyntasaException("App JSON NOT FOUND IN DIRECTORY", ErrorCode.GENERIC);
        }
    }*/


    /*public String getJsonResourceAsString(File file) throws SyntasaException {
        if(file != null && file.exists()) {
            try {
                return IOUtils.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new SyntasaException("DefaultSyntasaRole.json NOT FOUND IN DIRECTORY", ErrorCode.GENERIC);
        }
        return null;

        throw new SyntasaException("Unable to find file: " + filename + " from basePath: " + basePath, ErrorCode.GENERIC);
    }*/

    public String getFileAsString(String path) throws SyntasaException {
        String json = null;
        byte[] fileAsBytes;

        String basePath = System.getenv("CONFIG_FOLDER_PATH");

        if(StringUtils.isNotEmpty(basePath)) {
            basePath = basePath.endsWith("/") ? basePath : basePath + "/";
        }

        try {

            fileAsBytes = Files.readAllBytes(Paths.get(basePath + AWS_JSON));
            json = new String(fileAsBytes, StandardCharsets.UTF_8);

        } catch (IOException e) {

            throw new SyntasaException("Unable to find file: " + basePath + AWS_JSON + " from basePath: "
                    + basePath, ErrorCode.GENERIC);

        }

        return json;
    }






    public List<File> getFiles(List<String> fileNames) {
        List<File> retFiles = new ArrayList<>();

        String basePath = System.getenv("CONFIG_FOLDER_PATH");

        if(StringUtils.isNotEmpty(basePath)) {
            basePath = basePath.endsWith("/") ? basePath : basePath + "/";
        }

        for(String fileName : fileNames) {
            File file = new File(basePath + fileName);
            if(file.exists()) {
                log.info("Found file: " + file.getAbsolutePath());
                retFiles.add(file);
            }
        }

        return retFiles;
    }

    public String loadRequiredPolicies() {
        String basePath = System.getenv("CONFIG_FOLDER_PATH");
        basePath = basePath.endsWith("/") ? basePath : basePath + "/";
        String filePath = basePath + "aws-policies.txt";
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(filePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
