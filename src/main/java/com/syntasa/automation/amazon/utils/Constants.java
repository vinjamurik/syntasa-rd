/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.amazon.utils;

public class Constants {

    public static final String VPC = "vpc";
    public static final String IGW = "igw";
    public static final String EC2INSTANCE = "ec2Instance";
    public static final String CREATEDBY = "createdby";
    public static final String DATE_CREATED = "datecreated";
    public static final String NAME = "Name";
    public static final String PROJECT_ID = "project-id";
    public static final String PROJECT_ID_PREFIX = "syntasa-";

}
