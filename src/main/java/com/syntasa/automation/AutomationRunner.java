/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation;

import com.syntasa.automation.amazon.SyntasaAwsTest;
import com.syntasa.automation.google.SyntasaGoogleTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;


@Component
@Slf4j
@Lazy
public class AutomationRunner implements CommandLineRunner {

    @Autowired
    private SyntasaAwsTest syntasaAwsTest;

    @Autowired
    private SyntasaGoogleTest syntasaGoogleTest;

    private static final boolean isAws = false;
    private static String PROP_LOCATION = "classpath:application.properties";

    @Override
    public void run(String... args) throws Exception {

        if (StringUtils.isNotEmpty(System.getenv("CONFIG_FOLDER_PATH"))) {
            PROP_LOCATION = System.getenv("CONFIG_FOLDER_PATH");
        }

        syntasaAwsTest.create(PROP_LOCATION);
        /*
        try {
            if(isAws) {
                syntasaAwsTest.test();
            } else {
                syntasaGoogleTest.test();
            }
        } catch (SyntasaException se) {
            se.printStackTrace();
        }
        */
    }
}
