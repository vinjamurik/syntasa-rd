/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.google;

import com.syntasa.automation.exception.SyntasaException;
import com.syntasa.automation.google.service.*;
import com.syntasa.automation.google.utils.ConnectionUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Lazy
public class SyntasaGoogleTest {

    @Autowired
    private ConnectionUtility connectionUtility;
    @Autowired
    private GoogleGCloudAuthService googleGCloudAuthService;
    @Autowired
    private GoogleProjectService googleProjectService;
    @Autowired
    private GoogleApiService googleApiService;
    @Autowired
    private GoogleBillingService googleBillingService;
    @Autowired
    private GoogleServiceAccountService googleServiceAccountService;
    @Autowired
    private GoogleServiceAccountKeyService googleServiceAccountKeyService;
    @Autowired
    private GoogleRoleService googleRoleService;
    @Autowired
    private GoogleVpcService googleVpcService;
    @Autowired
    private GoogleSubnetService googleSubnetService;
    @Autowired
    private  GoogleFirewallService googleFirewallService;
    @Autowired
    private GoogleComputeService googleComputeService;
    @Autowired
    private GoogleDatabaseService googleDatabaseService;

    public void test() throws SyntasaException {
        //todo: add master user and password to database
        //todo: delete default network
        connectionUtility.loadEnvironment();
        googleGCloudAuthService.createGCloudConfiguration();
        googleGCloudAuthService.loginGCloud();
        //googleProjectService.createProject();

        googleBillingService.enableBillingApi();
        connectionUtility.initGoogleCredential();
        googleApiService.enableApis(connectionUtility.getEnv().getRequiredApis());
        googleServiceAccountService.createServiceAccount();
        googleRoleService.attachRolesToServiceAccount(connectionUtility.getEnv().getRequiredRoles());
        googleServiceAccountKeyService.createServiceAccountKey();
        System.exit(0);

        googleVpcService.createVpc();
        googleSubnetService.createSubnets(connectionUtility.getEnv().getRequiredSubnets());
        googleFirewallService.createFirewallRules(connectionUtility.getEnv().getRequiredFirewallRules());
        connectionUtility.initGoogleCredential();
        googleComputeService.createInstances(connectionUtility.getEnv().getRequiredInstances());
        googleDatabaseService.createDatabase(connectionUtility.getEnv().getRequiredDatabases());
        log.info("Successfully completed GCP automation program. Have a nice day!");
    }
}
