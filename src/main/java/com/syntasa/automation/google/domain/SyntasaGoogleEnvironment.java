package com.syntasa.automation.google.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class SyntasaGoogleEnvironment extends SyntasaDtoMarker {
    private String adminEmail;
    private String projectName;
    private String accountEmail;
    private String accountPassword;
    private List<String> requiredApis;
    private List<String> requiredRoles;
    private List<GoogleSubnet> requiredSubnets;
    private List<GoogleFirewallRule> requiredFirewallRules;
    private String gcpRegion;
    private String gcpZone;
    private List<GoogleInstance> requiredInstances;
    private List<GoogleDatabase> requiredDatabases;
}
