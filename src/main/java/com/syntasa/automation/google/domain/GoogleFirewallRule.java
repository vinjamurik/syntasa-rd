/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.google.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoogleFirewallRule {

    private String name;
    private String protocol;
    private String port;
    private String cidr;
    private boolean isIngress;

    @Override
    public String toString() {
        return "GoogleFirewallRule{" +
                "name='" + name + '\'' +
                ", protocol='" + protocol + '\'' +
                ", port='" + port + '\'' +
                ", cidr='" + cidr + '\'' +
                ", isIngress=" + isIngress +
                '}';
    }
}
