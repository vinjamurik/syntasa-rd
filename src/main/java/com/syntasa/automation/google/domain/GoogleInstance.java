/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.google.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoogleInstance {

    private String instanceName;
    private String imageFamily;
    private String imageProject;
    private String machineType;
    private String scopes;
    private String startupScript;
    private String zone;
    private String network;

    @Override
    public String toString() {
        return "GoogleInstance{" +
                "instanceName='" + instanceName + '\'' +
                ", imageFamily='" + imageFamily + '\'' +
                ", imageProject='" + imageProject + '\'' +
                ", machineType='" + machineType + '\'' +
                ", scopes='" + scopes + '\'' +
                ", startupScript='" + startupScript + '\'' +
                ", zone='" + zone + '\'' +
                ", network='" + network + '\'' +
                '}';
    }
}
