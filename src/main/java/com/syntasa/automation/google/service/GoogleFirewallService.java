/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.google.service;

import com.syntasa.automation.exception.SyntasaException;
import com.syntasa.automation.google.domain.GoogleFirewallRule;
import com.syntasa.automation.google.utils.CommandLineUtility;
import com.syntasa.automation.google.utils.ConnectionUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
@Lazy
public class GoogleFirewallService {

    @Autowired
    private ConnectionUtility connectionUtility;
    @Autowired
    private CommandLineUtility commandLineUtility;

    public void createFirewallRules(List<GoogleFirewallRule> rules) {

        for(GoogleFirewallRule rule : rules) {
            addFirewallRule(rule);
        }

    }

    public void addFirewallRule(GoogleFirewallRule rule) {

        try {
            String command = null;
            if(rule.isIngress()) {

                command = "gcloud compute firewall-rules create " +
                        rule.getName() + " --allow " + rule.getProtocol() + ":" + rule.getPort() +
                        " --source-ranges=" + rule.getCidr() +
                        " --project " + connectionUtility.getEnv().getProjectName() +
                        " --network=" + connectionUtility.getEnv().getProjectName() + "-vpc";

            } else {
                if(rule.getPort() != null && rule.getPort().length() > 1 &&
                        rule.getProtocol() != null && rule.getProtocol().length() > 1) {

                    command = "gcloud compute firewall-rules create " +
                            rule.getName() + " --allow" + rule.getProtocol() + ":" + rule.getPort() +
                            " --destination-ranges=" + rule.getCidr() +
                            " --project " + connectionUtility.getEnv().getProjectName() +
                            " --network=" + connectionUtility.getEnv().getProjectName() + "-vpc" +
                            " --direction=EGRESS";

                } else {

                    command = "gcloud compute firewall-rules create " +
                            rule.getName() + " --allow=all" +
                            " --destination-ranges=" + rule.getCidr() +
                            " --project " + connectionUtility.getEnv().getProjectName() +
                            " --network=" + connectionUtility.getEnv().getProjectName() + "-vpc" +
                            " --direction=EGRESS";

                }

            }

            commandLineUtility.runCommand(command);


        } catch (SyntasaException e) {
            e.printStackTrace();
        }

    }

}
