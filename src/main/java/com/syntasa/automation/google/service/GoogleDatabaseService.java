/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.google.service;

import com.google.api.services.sqladmin.SQLAdmin;
import com.google.api.services.sqladmin.model.DatabaseInstance;
import com.google.api.services.sqladmin.model.Operation;
import com.google.api.services.sqladmin.model.Settings;
import com.google.api.services.sqladmin.model.User;
import com.syntasa.automation.exception.SyntasaException;
import com.syntasa.automation.google.domain.GoogleDatabase;
import com.syntasa.automation.google.utils.CommandLineUtility;
import com.syntasa.automation.google.utils.ConnectionUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
@Slf4j
@Lazy
public class GoogleDatabaseService {

    @Autowired
    private ConnectionUtility connectionUtility;
    @Autowired
    private CommandLineUtility commandLineUtility;

    public void createDatabase(List<GoogleDatabase> databases) {
        for(GoogleDatabase db : databases) {
            addDatabase(db);
        }
    }

    public void addDatabase(GoogleDatabase db) {

        try {

            DatabaseInstance database = new DatabaseInstance();

            database.setProject(connectionUtility.getEnv().getProjectName());
            database.setRegion(connectionUtility.getEnv().getGcpRegion());
            database.setSettings(new Settings().setTier(db.getTier()));
            database.setDatabaseVersion(db.getVersion());
            database.setBackendType(db.getType());
            database.setName(db.getName());

            SQLAdmin.Instances.Insert request = connectionUtility.getSqlAdminConnection().instances()
                    .insert(connectionUtility.getPROJECT_ID(), database);

            Operation operation = request.execute();

            log.info("CREATE DATABASE REQUEST: Operation Status - " + operation.getStatus());

            User setRootPassword = new User().setPassword(db.getPassword());

            SQLAdmin.Users.Update updateRequest = connectionUtility.getSqlAdminConnection().users()
                            .update(connectionUtility.getEnv().getProjectName(), database.getName(),
                                    "@", "root", setRootPassword);

            Operation updateResponse = updateRequest.execute();

            log.info("SET DATABASE ROOT PASSWORD REQUEST: Database - " + database.getName());

        } catch (SyntasaException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
