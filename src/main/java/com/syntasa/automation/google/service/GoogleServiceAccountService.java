/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.google.service;

import com.syntasa.automation.exception.SyntasaException;
import com.syntasa.automation.google.utils.CommandLineUtility;
import com.syntasa.automation.google.utils.ConnectionUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Lazy
public class GoogleServiceAccountService {

    @Autowired
    private ConnectionUtility connectionUtility;

    @Autowired
    private CommandLineUtility commandLineUtility;

    String saName;
    String projectName;

    public void createServiceAccount() throws SyntasaException {
        saName = connectionUtility.getEnv().getProjectName() + "-management-sa";
        projectName = connectionUtility.getEnv().getProjectName();
        String createSa = "gcloud iam service-accounts create " + saName + " --display-name " + saName
                + " --project " + projectName;
        commandLineUtility.runCommand(createSa);
    }

}
