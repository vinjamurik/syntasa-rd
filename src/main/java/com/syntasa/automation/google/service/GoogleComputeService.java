/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.google.service;

import com.google.api.services.compute.model.*;
import com.syntasa.automation.exception.SyntasaException;
import com.syntasa.automation.google.domain.GoogleInstance;
import com.syntasa.automation.google.utils.CommandLineUtility;
import com.syntasa.automation.google.utils.ConnectionUtility;
import com.syntasa.automation.google.utils.FileUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@Slf4j
@Lazy
public class GoogleComputeService {

    @Autowired
    private ConnectionUtility connectionUtility;
    @Autowired
    private CommandLineUtility commandLineUtility;
    @Autowired
    private FileUtility fileUtility;

    public void createInstances(List<GoogleInstance> instances) {
        for(GoogleInstance instance : instances) {
            addInstance(instance);
        }
    }

    // IMAGE FAMILY LINK FOR REFERENCE: https://cloud.google.com/compute/docs/images
    public void addInstance(GoogleInstance instance) {
        try {
            Instance vm = new Instance();

            vm.setName(instance.getInstanceName());

            vm.setMachineType("https://www.googleapis.com/compute/v1/projects/" +
                            connectionUtility.getPROJECT_ID() + "/zones/" +
                            instance.getZone() + "/machineTypes/" + instance.getMachineType());

            vm.setZone(instance.getZone());

            String network = "https://www.googleapis.com/compute/v1/projects/" + connectionUtility.getPROJECT_ID() +
                    "/global/networks/" + connectionUtility.getEnv().getProjectName() + "-vpc";

            String subnet = "projects/" + connectionUtility.getPROJECT_ID() +
                    "/regions/" + connectionUtility.getEnv().getGcpRegion() +
                    "/subnetworks/" + instance.getNetwork();

            List<AccessConfig> configs = new ArrayList<>();
            NetworkInterface nic = new NetworkInterface().setNetwork(network).setSubnetwork(subnet);
            AccessConfig config = new AccessConfig().setType("ONE_TO_ONE_NAT").setName("External NAT");
            configs.add(config);

            nic.setAccessConfigs(configs);
            vm.setNetworkInterfaces(Collections.singletonList(nic));

            AttachedDisk disk = new AttachedDisk().setBoot(true).setAutoDelete(true).setType("PERSISTENT");
            AttachedDiskInitializeParams params = new AttachedDiskInitializeParams().setDiskName(instance.getInstanceName());

            String command = "gcloud compute images list --filter=\"family:(" + instance.getImageFamily() + ")\"";

            List<String> osVersions = commandLineUtility.runCommand(command);
            String os = osVersions.get(0).split("[ ]+")[0];

            log.info("OS VERSION SET ::: OS Version - " + os + " ::: FOR INSTANCE ::: Instance Name - " + instance.getInstanceName());

            String SOURCE_IMAGE_PATH = "https://www.googleapis.com/compute/v1/projects/" + instance.getImageProject() +
                    "/global/images/" + os;

            params.setSourceImage(SOURCE_IMAGE_PATH);
            params.setDiskType("https://www.googleapis.com/compute/v1/projects/" + connectionUtility.getPROJECT_ID() + "/zones/"
                    + instance.getZone() + "/diskTypes/pd-standard");
            disk.setInitializeParams(params);

            vm.setDisks(Collections.singletonList(disk));

            if(instance.getStartupScript() != null && instance.getStartupScript().length() > 1) {

                String script = fileUtility.readFileToString(instance.getStartupScript());

                vm.setMetadata(new Metadata().setItems(Collections.singletonList(
                        new Metadata.Items().setKey("startup-script").setValue(script))
                ));

            }


            connectionUtility.getComputeConnection().instances().insert(
                    connectionUtility.getPROJECT_ID(),
                    instance.getZone(),
                    vm).execute();

            log.info("CREATE INSTANCE REQUEST: Instance Name - " + vm.getName() + " Project - " + connectionUtility.getPROJECT_ID());
        } catch (SyntasaException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
