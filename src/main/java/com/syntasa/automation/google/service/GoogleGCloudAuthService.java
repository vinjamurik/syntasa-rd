/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.google.service;

import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import com.syntasa.automation.google.utils.CommandLineUtility;
import com.syntasa.automation.google.utils.ConnectionUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
@Lazy
public class GoogleGCloudAuthService {
    @Autowired
    private ConnectionUtility connectionUtility;
    @Autowired
    private CommandLineUtility commandLineUtility;

    public void createGCloudConfiguration() {

        String command = "gcloud config configurations create syntasa-config --activate";

        /**
         * Using the custom command runner because this command will fail if the configuration is already
         * created, which is okay. CommandLineRunner.runCommand() would throw an exception that would
         * interrupt a normal and functional command.
         */
        customCommandRunner(command);

    }

    public void loginGCloud() {
        String command = "gcloud auth login " + "jeffrey.day33@gmail.com --launch-browser";

        customCommandRunner(command);
    }

    public void customCommandRunner(String command) {
        try {
            String shellOutput;
            log.info("Running process at command line. ::: " + command);
            ProcessBuilder builder = null;

            if(System.getProperty("os.name").contains("Windows")) {
                builder = new ProcessBuilder("cmd.exe", "/c", command);
            } else {
                builder = new ProcessBuilder("bash", "-c", command);
            }
            builder.redirectErrorStream(true);
            Process process = builder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            if(process.waitFor(300, TimeUnit.SECONDS)) {
                while ((shellOutput = reader.readLine()) != null) {
                    log.info("Process response :::: " + shellOutput);
                }
                reader.close();
            }

            if(process.isAlive()) {
                process.destroy();
                log.error("Process failed to complete at command line ::: Command = " + command);
                throw new SyntasaException("Failed to complete process at command line. ::: Command = " + command,
                        ErrorCode.GENERIC);
            }

            log.info("Completed process at command line. ::: Command = " + command);

        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        } catch (SyntasaException e) {
            e.printStackTrace();
        }
    }
}
