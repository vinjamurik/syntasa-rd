/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */
package com.syntasa.automation.google.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.syntasa.automation.exception.SyntasaException;
import com.syntasa.automation.google.domain.SyntasaGoogleEnvironment;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@Component
@Slf4j
@Lazy
@Getter
public class FileUtility {

    public static final String GOOGLE_JSON = "google-application.json";
    public static final String GOOGLE_API_TXT = "google-apis.txt";

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private ObjectMapper objectMapper;
    private Map<String, String> apiMap;

    public SyntasaGoogleEnvironment getEnvironment() throws SyntasaException, IOException {
        String basePath = System.getenv("CONFIG_FOLDER_PATH");
        basePath = basePath.endsWith("/") ? basePath : basePath + "/";
        String path = basePath + GOOGLE_JSON;
        return objectMapper.readValue(new File(path), SyntasaGoogleEnvironment.class);
    }

    @PostConstruct
    public void loadApiMap() {
        try {
            String basePath = System.getenv("CONFIG_FOLDER_PATH");
            basePath = basePath.endsWith("/") ? basePath : basePath + "/";
            apiMap = new HashMap<>();
            BufferedReader reader = new BufferedReader(new FileReader(basePath + GOOGLE_API_TXT));
            reader.lines().forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                    String value = s.split(",")[0];
                    String key = s.split(",")[1];
                    apiMap.put(key,value);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String readFileToString(String filePath) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(filePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
