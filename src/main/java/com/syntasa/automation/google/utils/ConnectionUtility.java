/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.google.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.cloudbilling.Cloudbilling;
import com.google.api.services.cloudresourcemanager.CloudResourceManager;
import com.google.api.services.cloudresourcemanager.CloudResourceManagerScopes;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.ComputeScopes;
import com.google.api.services.sqladmin.SQLAdmin;
import com.google.api.services.sqladmin.SQLAdminScopes;
import com.google.api.services.storage.StorageScopes;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import com.syntasa.automation.google.domain.SyntasaGoogleEnvironment;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

@Component
@Slf4j
@Lazy
@Getter
public class ConnectionUtility {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private FileUtility fileUtility;

    private GoogleCredentials credentials;
    private GoogleCredential credential;
    private String PROJECT_ID;
    private CloudResourceManager manager;
    private Cloudbilling cloudbilling;
    private Storage storageConnection;
    private Compute computeConnection;
    private SQLAdmin sqlAdmin;
    private SyntasaGoogleEnvironment env;
    private String basePath;


    public void loadEnvironment() throws SyntasaException {

        try {
            this.env = fileUtility.getEnvironment();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initGoogleCredential() {
        try {
            String basePath = System.getenv("CONFIG_FOLDER_PATH");
            basePath = basePath.endsWith("/") ? basePath : basePath + "/";
            String path = basePath + env.getProjectName() + "-key.json";

            ClassLoader classLoader = getClass().getClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream(path);

            credential = GoogleCredential.fromStream(new FileInputStream(path)).createScoped(getDefaultScopes());

            System.out.println("CREDENTIAL: " + credential.getServiceAccountUser() + " " + credential.getServiceAccountId());
            PROJECT_ID = credential.getServiceAccountProjectId();

            credentials = GoogleCredentials.fromStream(new FileInputStream(path)).createScoped(getDefaultScopes());

            log.info("INITIALIZED CREDENTIALS ::: Account Name - " + credential.getServiceAccountId()
                    + " ProjectID - " + credential.getServiceAccountProjectId());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CloudResourceManager getCloudResourceManager() {
        if(this.manager == null) {
            HttpTransport httpTransport = new NetHttpTransport();
            JsonFactory jsonFactory = new JacksonFactory();
            manager = new CloudResourceManager.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName("SYNTASA-AUTOMATION-MANAGER").build();
        }
        return manager;
    }

    public Cloudbilling getCloudBillingService() {
        if(this.cloudbilling == null) {
            HttpTransport httpTransport = new NetHttpTransport();
            JsonFactory jsonFactory = new JacksonFactory();
            cloudbilling = new Cloudbilling.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName("SYNTASA-AUTOMATION-MANAGER").build();
        }
        return cloudbilling;
    }

    public Compute getComputeConnection() throws SyntasaException {
        if(this.computeConnection == null) {
            HttpTransport httpTransport = new NetHttpTransport();
            JsonFactory jsonFactory = new JacksonFactory();
            computeConnection = new Compute.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName("SYNTASA-AUTOMATION-MANAGER").build();
        }
        return computeConnection;
    }

    public Storage getStorageConnection() throws SyntasaException {
        if (this.storageConnection == null) {
            storageConnection = StorageOptions.newBuilder().setCredentials(credentials).setProjectId(PROJECT_ID)
                    .build().getService();
            return storageConnection;
        }
        return storageConnection;
    }

    public SQLAdmin getSqlAdminConnection() {
        if(this.sqlAdmin == null) {
            HttpTransport httpTransport = new NetHttpTransport();
            JsonFactory jsonFactory = new JacksonFactory();
            sqlAdmin = new SQLAdmin.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName("SYNTASA-AUTOMATION-MANAGER").build();
        }
        return sqlAdmin;
    }

    public Set<String> getDefaultScopes() {
        Set<String> scopes = new HashSet<>();
        //scopes.addAll(DataprocScopes.all());
        scopes.addAll(SQLAdminScopes.all());
        scopes.addAll(StorageScopes.all());
        scopes.addAll(ComputeScopes.all());
        scopes.addAll(CloudResourceManagerScopes.all());

        return scopes;
    }

    public String getFormattedProjectBasePath() throws SyntasaException {
        if(basePath == null) {
            basePath = System.getenv("CONFIG_FOLDER_PATH");

            if(StringUtils.isNotEmpty(basePath)) {
                basePath = basePath.endsWith("/") ? basePath : basePath + "/";
            }
        }
        return basePath;
    }

    public SyntasaGoogleEnvironment getEnv() throws SyntasaException {
        if(env == null)
            throw new SyntasaException("Must call initialize method before calling services.", ErrorCode.GENERIC);
        return env;
    }


}
