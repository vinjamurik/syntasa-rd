/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Lazy;

@SpringBootApplication
@Slf4j
@Lazy
public class Automation {

    private static String PROP_LOCATION = "classpath:application.properties";

    public static void main(String[] args) {

        if (StringUtils.isNotEmpty(System.getenv("CONFIG_FOLDER_PATH"))) {
            PROP_LOCATION = System.getenv("CONFIG_FOLDER_PATH");
        }

        System.out.println("This is the Properties File Location: " + PROP_LOCATION);

        SpringApplication springApplication = new SpringApplicationBuilder(Automation.class)
                .web(false)
                .properties("spring.config.location:" + PROP_LOCATION)
                .build();
        springApplication.run(args);
    }
}
