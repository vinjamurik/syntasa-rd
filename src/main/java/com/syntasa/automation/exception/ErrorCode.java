/*
 *  ***********************************************************************
 *   <p>
 *   NOTICE:
 *   <p>
 *   SYNTASA CONFIDENTIAL
 *   __________________
 *   <p>
 *   [2002] - [2014] SYNTASA Incorporated
 *   All Rights Reserved.
 *   <p>
 *   NOTICE:  All information contained herein is, and remains
 *   the property of SYNTASA Incorporated and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to SYNTASA Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from SYNTASA Incorporated.
 *   <p>
 * ***********************************************************************
 */

package com.syntasa.automation.exception;

public enum ErrorCode  {

    GENERIC(1000),
    IO_EXCEPTION(1001),
    BAD_FILE_EXCEPTION(1002),
    NO_INPUT_FILE(1003),
    FILE_NOT_FOUND(1004),
    STORAGE_WRITE_PERMISSION_DENIED(1005),
    URI_SYNTAX_EXCEPTION(1006),
    PARSE_EXCEPTION(1007),
    MESSAGE_PRODUCER_EXCEPTION(1008),
    INFRA_UNABLE_TO_FIND_ACTION(1009),
    INFRA_AWS_NOT_INITIALIZED(1010),
    INFRA_GCP_CREDENTIALS_NOT_FOUND(1011),
    INFRA_GCP_CONNECTION_ERROR(1012),
    FILESYSTEM_UNABLE_TO_CREATE(1013),
    INFRA_AWS_CONNECTION_ERROR(1014),
    INTERRUPTED_EXCEPTION(1015),
    INFRA_GCP_GENERIC(1016),
    INFRA_AWS_GENERIC(1017),
    BIGQUERY_UNABLE_TO_INSTANTIATE(1018),
    BIGQUERY_INVALID_DATASETSTATE(1019),
    UNSUPPORTED_COMPRESSION_TYPE(1020),
    UNSUPPORTED_ENCODING_TYPE(1021),
    UNSUPPORTED_FEATURE(1022),
    MISSING_HOUR_COL(1023),
    STREAM_EXCEPTION(1024),
    STATE_DATA_NOT_FOUND(1025),
    STATE_RESPONSE_EXCEPTION(1025),
    COPY_PROCESSOR_EXCEPTION(1026),
    DESERIALIZATION_EXCEPTION(1027),
    INVALID_DATA_TYPE(1028),
    PRODUCER_EXCEPTION(1029),
    SECURITY_EXCEPTION(1030),
    CREATE_USER_EXCEPTION(1031);

    private final int errorCode;

    ErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
