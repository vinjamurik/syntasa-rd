/*
 * NOTICE:
 * SYNTASA CONFIDENTIAL
 * __________________
 *
 * [2002] - [2014] SYNTASA Incorporated
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of SYNTASA Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SYNTASA Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SYNTASA Incorporated.
 */

package com.syntasa.automation.exception;

public class SyntasaRunTimeException extends RuntimeException {

    private final ErrorCode code;

    public SyntasaRunTimeException(ErrorCode code) {
        super();
        this.code = code;
    }

    public SyntasaRunTimeException(String message, Throwable cause, ErrorCode code) {
        super(message, cause);
        this.code = code;
    }

    public SyntasaRunTimeException(String message, ErrorCode code) {
        super(message);
        this.code = code;
    }

    public SyntasaRunTimeException(Throwable cause, ErrorCode code) {
        super(cause);
        this.code = code;
    }

    public ErrorCode getCode() {
        return this.code;
    }
}
