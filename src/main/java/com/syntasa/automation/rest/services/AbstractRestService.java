package com.syntasa.automation.rest.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.syntasa.automation.amazon.service.*;
import com.syntasa.automation.amazon.utils.ConnectionUtils;
import com.syntasa.automation.amazon.utils.FileUtils;
import com.syntasa.automation.rest.domain.ResourceNotFoundException;
import com.syntasa.automation.rest.domain.SyntasaRestErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class AbstractRestService {

    @Autowired
    protected SyntasaCredentialsService syntasaCredentialsService;
    @Autowired
    protected ObjectWriter objectWriter;
    @Autowired
    protected ConnectionUtils connectionUtils;
    @Autowired
    protected ResourceLoader resourceLoader;
    @Autowired
    protected SyntasaVpcService syntasaVpcService;
    @Autowired
    protected SyntasaIgwService syntasaIgwService;
    @Autowired
    protected SyntasaSubnetService syntasaSubnetService;
    @Autowired
    protected SyntasaRouteTableService syntasaRouteTableService;
    @Autowired
    protected SyntasaServiceAccountService syntasaServiceAccountService;
    @Autowired
    protected SyntasaAccessKeyService syntasaAccessKeyService;
    @Autowired
    protected SyntasaKeyPairService syntasaKeyPairService;
    @Autowired
    protected SyntasaRoleService syntasaRoleService;
    @Autowired
    protected SyntasaInstanceProfileService syntasaInstanceProfileService;
    @Autowired
    protected SyntasaSecurityGroupService syntasaSecurityGroupService;
    @Autowired
    protected SyntasaS3BucketService syntasaS3BucketService;
    @Autowired
    protected SyntasaEc2InstanceService syntasaEc2InstanceService;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected FileUtils fileUtils;

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public String handleResourceNotFoundException(ResourceNotFoundException ex) {

        try {
            return objectWriter.writeValueAsString(SyntasaRestErrorResponse.builder()
                    .error(true)
                    .endpoint(ex.getEndpoint())
                    .errorCode(ex.getCode())
                    .errorCodeValue(ex.getCode().getErrorCode())
                    .errorMessage(ex.getMessage()).build());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
