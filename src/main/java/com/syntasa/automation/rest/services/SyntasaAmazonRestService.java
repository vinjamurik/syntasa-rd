package com.syntasa.automation.rest.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.syntasa.automation.amazon.domain.AwsCredential;
import com.syntasa.automation.amazon.domain.AwsCredentialServiceResponse;
import com.syntasa.automation.amazon.domain.SyntasaAwsEnvironment;
import com.syntasa.automation.exception.ErrorCode;
import com.syntasa.automation.exception.SyntasaException;
import com.syntasa.automation.rest.domain.ResourceNotFoundException;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/amazon")
public class SyntasaAmazonRestService extends AbstractRestService {

    private static final String CRED_ENDPOINT = "validate/credentials";

    @RequestMapping(method = RequestMethod.POST, path = CRED_ENDPOINT)
    public String validateCredentials(@RequestBody AwsCredential json) throws ResourceNotFoundException {
        //Validate Request, otherwise throw a custom error message
        validateCredentialsRequest(json);

        //If Validation passed, then handle the request
        AwsCredentialServiceResponse response = null;

        try {
            response = syntasaCredentialsService.validateCredentials(json);
            return objectWriter.writeValueAsString(response);
        } catch (SyntasaException | JsonProcessingException e) {
            e.printStackTrace();
        }

        throw new ResourceNotFoundException("Unknown exception occurred, please contact the system administrator", ErrorCode.GENERIC, CRED_ENDPOINT);
    }



    @RequestMapping(method = RequestMethod.POST, path = "create")
    public String buildProject(@RequestBody SyntasaAwsEnvironment json){

        try {
            connectionUtils.loadEnvironment(json);

            syntasaVpcService.buildVpc();
            //syntasaIgwService.createSyntasaIgw();
            //syntasaSubnetService.buildSubnets();
            //syntasaRouteTableService.buildRouteTables();
            syntasaServiceAccountService.buildServiceAccount();

            String privateKey = syntasaKeyPairService.createSyntasaKeyPair();

            syntasaS3BucketService.buildSyntasaS3Bucket();
            syntasaS3BucketService.saveStringToFile(privateKey);

            syntasaInstanceProfileService.buildSyntasaInstanceProfile();
            //syntasaSecurityGroupService.buildSecurityGroup();

            //syntasaEc2InstanceService.createSyntasaEc2Instance();

        } catch (SyntasaException se) {
            deleteProject(json);
        }

        String response;
        try {
            response = objectWriter.writeValueAsString(json);
            return response;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "Complete";
    }

    @RequestMapping(method = RequestMethod.POST, path = "delete")
    public String deleteProject(@RequestBody SyntasaAwsEnvironment json) {
        try {
            connectionUtils.loadEnvironment(json);

            //syntasaEc2InstanceService.terminateEc2Instance();
            //syntasaSecurityGroupService.deleteSecurityGroup();
            syntasaInstanceProfileService.deleteInstanceProfile();
            syntasaS3BucketService.deleteSyntasaS3Bucket();
            syntasaKeyPairService.deleteKeyPair();
            syntasaServiceAccountService.deleteServiceAccount();

            syntasaSubnetService.deleteSyntasaSubnets();
            syntasaRouteTableService.deleteSyntasaRouteTables();
            //syntasaVpcService.deleteVpc();

        } catch (SyntasaException e) {
            e.printStackTrace();
        }
        return "Deleted";
    }

    public void validateCredentialsRequest(AwsCredential json) throws ResourceNotFoundException {
        boolean isValid = false;
        String retMessage = "";

        if(json == null) {
            retMessage = "AWS Credentials JSON sent to server was invalid, please check the JSON and try again.";
        } else if(StringUtils.isEmpty(json.getAccessKey())) {
            retMessage = "AWS Credentials JSON sent to server did not contain the Administrator's Access Key, please check the JSON and try again.";
        } else if(StringUtils.isEmpty(json.getSecretKey())) {
            retMessage = "AWS Credentials JSON sent to server did not contain the Administrator's Secret Key, please check the JSON and try again.";
        } else if(StringUtils.isEmpty(json.getRegion())) {
            retMessage = "AWS Credentials JSON sent to server did not contain the Administrator's Region, please check the JSON and try again.";
        } else {
            isValid = true;
        }

        if(!isValid) {
            throw new ResourceNotFoundException(retMessage, ErrorCode.GENERIC, CRED_ENDPOINT);
        }
    }


}
