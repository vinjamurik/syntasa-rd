package com.syntasa.automation.rest.domain;

import com.syntasa.automation.exception.ErrorCode;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SyntasaRestErrorResponse {

    private boolean error;
    private ErrorCode errorCode;
    private int errorCodeValue;
    private String errorMessage;
    private String endpoint;
}
