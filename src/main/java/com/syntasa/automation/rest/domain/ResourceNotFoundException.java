package com.syntasa.automation.rest.domain;

import com.syntasa.automation.exception.ErrorCode;

public class ResourceNotFoundException extends Exception {

    private final ErrorCode code;
    private final String endpoint;

    public ResourceNotFoundException(ErrorCode code, String endpoint) {
        super();
        this.code = code;
        this.endpoint = endpoint;
    }

    public ResourceNotFoundException(String message, Throwable cause, ErrorCode code, String endpoint) {
        super(message, cause);
        this.code = code;
        this.endpoint = endpoint;
    }

    public ResourceNotFoundException(String message, ErrorCode code, String endpoint) {
        super(message);
        this.code = code;
        this.endpoint = endpoint;
    }

    public ResourceNotFoundException(Throwable cause, ErrorCode code, String endpoint) {
        super(cause);
        this.code = code;
        this.endpoint = endpoint;
    }

    public ErrorCode getCode() {
        return this.code;
    }
    public String getEndpoint() { return this.endpoint; }
}
