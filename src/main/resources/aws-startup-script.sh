#!/bin/bash

sudo yum install wget -y
sudo wget --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/jdk-8u181-linux-x64.tar.gz
sudo mkdir /etc/jdks

sudo tar xzf jdk-8u181-linux-x64.tar.gz -C /etc/jdks
sudo alternatives --install /usr/bin/java java /etc/jdks/jdk1.8.0_181/bin/java 1
sudo alternatives --install /usr/bin/jar jar /etc/jdks/jdk1.8.0_181/bin/jar 1
sudo alternatives --install /usr/bin/javac javac /etc/jdks/jdk1.8.0_181/bin/javac 1
sudo alternatives --set jar /etc/jdks/jdk1.8.0_181/bin/jar
sudo alternatives --set javac /etc/jdks/jdk1.8.0_181/bin/javac

sudo rm -rf jdk-8u181-linux-x64.tar.gz

#Turn off Selinux
sudo echo 0 > /etc/selinux/enforce
sudo setenforce 0
sudo sed -i 's/enforcing/disabled/g' /etc/selinux/config /etc/selinux/config

#Install YUM Packages
sudo yum update -y && yum upgrade -y  && yum install epel-release -y

sleep 1

sudo yum install ftp python-pip docker jq -y

#Add SMA user and Docker Group.  Add SMA to both groups
sudo useradd sma
sudo passwd -d sma
sudo groupadd docker
sudo usermod -aG wheel sma
sudo usermod -aG docker sma

#Install AWS CLI
sudo pip install awscli 
sudo pip install awscli --upgrade

#Install docker-compose
sudo sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#Enable New Services
sudo systemctl enable docker

#Start Services
sudo systemctl start docker

sleep 5

sudo echo -e "sma\tALL=(ALL)\tALL" | sudo tee --append /etc/sudoers

#Login to Docker using SMA User
sudo su sma -c 'docker login --username=syntasadocker --password=Cyn@Dock2018'

#Set Machine Hostname
sudo hostnamectl set-hostname syntasa-adidas-app

#Set JAVA_HOME 
sudo cat <<'EOF' > /etc/profile.d/syntasa.sh
#!/bin/bash

export JAVA_HOME=/etc/jdks/jdk1.8.0_181
export PATH=$PATH:$JAVA_HOME/bin

EOF

#Configure AWS Settings
sudo su sma -c 'aws configure set aws_access_key_id AKIAJ3GZ3SZ6Z4DZ4MRQ'
sudo su sma -c 'aws configure set aws_secret_access_key 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'
sudo su sma -c 'aws configure set default.region eu-central-1'
sudo su sma -c 'aws s3 cp s3://syn-adidas/syn-installation-files/ /home/sma/ --recursive'
sudo su sma -c 'tar -xvf /home/sma/*.tar.gz -C /home/sma'
sudo su sma -c 'chmod +x /home/sma/*.sh'
sudo su sma -c 'chown sma:sma /home/sma/*'

#Install Syntasa backend
#sudo su sma -c 'bash /home/sma/sma-data/bin/install-syntasa-data.sh -v4.0.1'

sleep 1

#Restart the Machine
sudo reboot now

exit 0