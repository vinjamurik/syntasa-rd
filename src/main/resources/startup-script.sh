#!/bin/bash

sudo yum install wget -y
sudo wget --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/jdk-8u181-linux-x64.tar.gz
sudo mkdir /etc/jdks

sudo tar xzf jdk-8u181-linux-x64.tar.gz -C /etc/jdks
sudo alternatives --install /usr/bin/java java /etc/jdks/jdk1.8.0_181/bin/java 1
sudo alternatives --install /usr/bin/jar jar /etc/jdks/jdk1.8.0_181/bin/jar 1
sudo alternatives --install /usr/bin/javac javac /etc/jdks/jdk1.8.0_181/bin/javac 1
sudo alternatives --set jar /etc/jdks/jdk1.8.0_181/bin/jar
sudo alternatives --set javac /etc/jdks/jdk1.8.0_181/bin/javac

sudo rm -rf jdk-8u181-linux-x64.tar.gz