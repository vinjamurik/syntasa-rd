#!/bin/bash

# Initialize environment
## Add databases to postgres
## Add initialize database
## Start up containers

color='\033[0;36m'
green='\033[0;32m'
clear='\033[0m'
red='\033[0;31m'
bold='\033[0;1m'
version="latest"
force=true

while getopts ":fv:" opt; do
  case $opt in
    v)
      version="$OPTARG"
      ;;
    f)
      force=true
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

export VERSION="$version"

echo -e "${green}
@@@@@@@@@@@@@@@@@@@@@@@
@@@*      #@@    @@@@@@
@*          @@    #@@@@
@    *@@@@@@@@@    @@@@   ${clear}SYNTASA${green}
@@         *@@@@    #@@   ${clear}Version:${version}${green}
@@@@@@@      @@@@    #@
@    *@@*    @@@@@    @
@@@        @@@@@@@    @
@@@@@@@@@@@@@@@@@@@@@@@
${clear}"

fresh_install () {
  #Install syntasa app
  echo -e "${color}Starting SYNTASA app ${version} installation...${clear}"
  echo -ne "${color}Creating service config folders${clear}"

  #Extract tar data
  ARCHIVE=$(awk '/^__ARCHIVE__/ {print NR + 1; exit 0; }' "${0}")
  tail -n+${ARCHIVE} "${0}" | tar xz -C "/home/sma/"

  chown -R sma:sma app_conf
  echo -ne " ... ${green}done${clear}"\\n

  echo -e "${color}Creating databases${clear}"
  docker exec -itu postgres syntasa-postgres psql -c 'CREATE DATABASE sma_business;'
  docker exec -itu postgres syntasa-postgres psql -c 'CREATE DATABASE sma_platform;'
  docker exec -itu postgres syntasa-postgres psql -c 'CREATE DATABASE sma_preview;'

  docker exec syntasa-postgres psql -U admin -d postgres -c "CREATE USER zoomdata WITH PASSWORD 'zoomdatapassword';"
  docker exec -itu postgres syntasa-postgres psql -c 'CREATE DATABASE "zoomdata-upload" WITH OWNER zoomdata;'
  docker exec -itu postgres syntasa-postgres psql -c 'CREATE DATABASE "zoomdata-keyset" WITH OWNER zoomdata;'
  docker exec -itu postgres syntasa-postgres psql -c 'CREATE DATABASE "zoomdata-scheduler" WITH OWNER zoomdata;'
  docker exec -itu postgres syntasa-postgres psql -c 'CREATE DATABASE "zoomdata" WITH OWNER zoomdata;'

  echo -e "${color}Downloading Docker images${clear}"
  cd app_conf
  /usr/local/bin/docker-compose -f docker-compose-app.yml pull

  echo -ne "${color}Initializing databases${clear}"
  docker run --rm --name business-temp --network=docker_syntasa_network -v "$(pwd)"/business/config:/syn-app/server/services/business/config --entrypoint=./initialize_database.sh docker.io/syntasadocker/syntasa-app-business
  docker run --rm --name platform-temp --network=docker_syntasa_network -v "$(pwd)"/platform/config:/syn-app/server/services/platform/config --entrypoint=./initialize_database.sh docker.io/syntasadocker/syntasa-app-platform
  # docker run --rm --name preview-temp --network=docker_syntasa_network -v "$(pwd)"/preview/config:/syn-app/server/services/preview/config --entrypoint=./initialize_database.sh docker.io/syntasadocker/syntasa-app-preview
  echo -ne " ... ${green}done${clear}"\\n

  echo -e "${color}Creating Docker containers${clear}"
  cd /home/sma/app_conf
  # docker-compose -f docker-compose-app.yml up -d
  sudo -E /usr/local/bin/docker-compose -f docker-compose-app.yml up -d
}


if [ ! -d "/home/sma/app_conf" ]; then
    safety_check=true
    if $force ; then
      safety_check=false
    fi

    if $safety_check ; then
      echo -ne "${red}New installation detected. We will attempt to create databases and initialize them. If run \
on an existing installation, data loss WILL occur. Are you sure you want to continue? Type \"YES\" followed by [ENTER] to continue:  ${clear}"
      read first_confirm
      if [ "$first_confirm" == "YES" ]; then
        fresh_install
      else
        echo -ne "${red}EXITING...${clear}"
        exit 1
      fi
    else
      fresh_install
    fi

else
    #Upgrade syntasa app
    # current_version="$(docker inspect --format "{{ index .Config.Labels \"version\"}}" syntasadocker/syntasa-app-apigateway)"
    echo -e "${color}Starting SYNTASA app upgrade...${clear}"
    echo -e "${color}Downloading Docker images${clear}"
    cd app_conf
    docker-compose -f docker-compose-app.yml pull

    # new_version="$(docker inspect --format "{{ index .Config.Labels \"version\"}}" syntasadocker/syntasa-app-apigateway)"

    echo -e "${color}Creating Docker containers${clear}"
    docker-compose -f docker-compose-app.yml up -d

    echo -e "${color}Removing old images${clear}"
    docker rmi $(docker images -q -f dangling=true)
fi


echo -e "${color}All done!${clear}"
exit 0
__ARCHIVE__
� -�q[ �Yi��V��g~E��~c�ٷ�"]vcc/�_"v0�Y�������$3�I&��v��:u�S����Y�A?�}D���P8��\�O��a��$L�?�J��Ow�����eeww?��4
��7徶�/%�3e�r[���C��?���~��]�K���C���?�����0E���{������p*����1L��o���F�;��9����s�����(�,��tw�?��r�a�YY�f�qf���xZ��|`���775��uFU��Gf�����_��/�yf\�Sć�x�e����I5x��>,Dn7:g�EUB×��M�?<����e�������/.Z7�qf����l٥�Y��+���ڊC��TY�ы@6C6��Dݿll�U�Uh����?�PVŠ�w/��l4v����u_e�'B�!�d ��?a4�zV�:B���$a�?O>=�Y�V�/\gtk dT������
��1W�fa�Y^#�N���������7�5�0 ����Y���Z�����`a�ϟ_��,v�C�
S����͍'���]ͱ�����(��qWV�G�v#P�c�5��>y֥oXO����9ݢ	m�M(�AX�q��ף��浌��$K�̱^�Jn��c8l�����B�/)���G^�M_st�CPj�e�)�_���(�U;2���o�' ?>.�����j�ϲ�1�#y�U��]��1�?���9V�P%�Y�_��9(F�+�/�fY�Ay�)�b��ri&�ܭ�'��{��m`��į~'N��r�eQ��4����4����8��Az}��Ƣ����_�wӏuy#���A=2��x̦���$�pӄo*ǐKas��Cs�0/�_
Uῲ��Đ�y�y~9���ћ[x*2Wo�̈ܧ+��	�ڌ��Z� g  �K��+1��5-ߵ���5��e�_���oи���f�l�*��t����m9�g�q%��59o~)ݲ�-�O�T��Э�о�h}S
�l]Sۼܿqn�6\b8���3\^��l�xߺ�8�U��*����
�J�:N1�MV��(e��#l�KY��ڍp�@{I���Gy7mF��mn��ɠ��[>D�͘1l��E>�/{�*�CȻ�Qt�Eѡ��c�AY��b���3<��]�pr�T�뉦w�a�(qL\P�e�.��`�q���+�ݓ�C���q������>��h�]�w��/����_�������I�?���A�>��q��{my?�w7Eh��i96���X�Ƃ�Ϊ!��^��+�;�q��8����J��8v��kۿ{컙_B��zϘ��d�>j���c7�à�^�
����I��i�e90S'vŋ���#<���qH�2���~嬃v�C���+���׽��Źմ����߈��N�E,���6z��?{5�>��V|�;��K��
�_���K������/�:�_�?�o�%1�	�"~��=��w�����������o }�?�!���{�Ǒ8QVw�{�����f��hEf�v���� .��Aۊ ���J�e��q���+0��(��Bq K:iy�B4:��>��r��(^����E98]�'G�i�!�n����<����	Ϸ�)4QŘ[8O*��[���K>�����|��}�����֬ˣ��v��&�}�O:ܪ�!�
�^9'C�mN=wr���}"��iⲢ���vs��m �q͘
.���v�9�U�V��Z�@��f�^h��ׅ�MDK�Yf����N1�D�9�)�d-�䆝�3h����R+A�n�?C��)D'mG'`*�DYЛs�#���*Zd]��+�<Y2��Z���~&�cА\%�%
�t{>�<���ˤA�}XLG0\�s�1�f��f
yE����Dl��s",���6���ͷ%M�%�:�n��:������q�s�����EG=�/ ͽ�Ƕ�!��I]�&0�+"m r`��ܝ/OT$��&�ͰU�2�<����s�:�8��^�-z����P&�v���Z4�-ş�p��ټ.��1t�I��h5�T�6���D1kʫ�d��Vͻ�q�~���Xl)B^i�$6)8Y���ow�%ZK�0�x	s���*{�*�B �S��ↂ��5yِ����d���E�*��c�n�t����X����5H�<*�fe�1i�Y��x�i]&�Bo�~�c6{o��-ȅ�2�W�UH�%h�)��k2�Q̒]-pj7�}M���r�>?����ȗ��;5�,׈�96�-�S�G�z	��&�[��-L����6�Q'�Ŷ����OB�V�f���O'(�����lkǙ��*�LS$�3.h��Cv�21IY�$�Hȧk���-j� 絚�+��%�;嵾ڮ��&�	ɢλ�K�~�(�T6%����s�U")[lvҁ�V��nq>TiV.Y��4��ċ(j30Q)���5�(��I���B�0�'���d^���+y�s`㸵)�J��#m���%L�@��Sx�Ts{��ِs�ʫ`髎��r��eԝ�ɥ_��oՙ_�g�4�B�H��Jgrb��{�S���Y����9_$�3�#chː�f�1�^`0�-v����n]�y�x�/���g ^WbA>#rc�<��f:�p_�łX�9O����P�����ԭ�CL�i-r����?n��ҧ�˪/헩�d�7�hc0M8�3-��F�dxh^��f
OD)��@5��AD�&�)���	k0���j�j�vzQ�4]B.L��;-�j��Q���"+���\�,�6������kw�����f���C��$ţ�U2qșt��Y�xp���v&q!|�c����E�5������?��A��G`0��07�~)!��ןF�e+1tw*�	H��4:�vbԕ��p���FAѐ�D�[Id��+�Du�C���bNpd�X>�3�����K�:t;u��E��pQ*qG��#f҉T*��%�5�I��+Iز�Ɍd@��)���[�P�����>w0u�T��:�aJIS@�峉Ѩ����H�AR��G]gÜ�n�>ޭ%��r���q��c��4`�r��V58'uJ>[�∀"c�o���МMj9@��;�,�;�b3OVӡe�*��j:1|K_��|~�ó�y�D����\���F��w�K�Eh:A�ʞ1����]#�0��S��k�E뻬�tC�e��o&SB��u��X��������3�[F@Ԩ��f��0s�^f�~��Mf���9!�wk[K���i���G�:Tݚ3�D�tzYX"��ۅV5ni�N���u?\:59���g~-��up�̡��o�9t>�ɬ� �W0��Q-L��\Ϥ��&\tBQ�v�$͗!�T���������ì/���
)�F�J��h:i�K�l�)�ș*wl�e�:�-UF/@Y��J��#�
7���mö����\�>Ɔ+EC��>���_�����A��ꁆ(�%��pgͷA.j&/�r�tr��q0R�K�0gqxV�*p����~��4���8mwg<o�R�Ok��(�jF�gY�O�IJYL���\x1�sZ-si>�(�Yb��Fv�~�o	����sq�#LjyI@l]tR#f&ܴ7��X=� }BZI��Ap�p��I��G(fbNo�<��GUT�&��T��i������ڲ w.�NZ0�9��ć��r��Fa�N ���C�3���@cT��-{��}����=�{1�mIq}��y�pL�v�R4��vI�nLk�n�3�d��C������`��+�D��/�Cf��iu&f�R&�Pڵ0L��@�xs=�M(�����j�BP"�lc|�AӲ;f�Vn���'mn[G���Wp���{ώuZ>*ɔn˺/��Ԭ�"!�/�е���v��E���(ʛ)���B�t�э�N�~h�J�S�<�WW�I��]G��q�T�����ri���p9�r��	q�y{>�Qf�H���c�!���rN�>j�Z�3�ܳD�ݙK$�J��,~>MW��rq�%�v�Z��ZC�}�<��Q�,��XG5��J�j'�b�Dy�ҳ�X:R�I���<X�l�꾠>[BoҬ�w������*_/SeR�̧Z�*�Y?7Z�ȼZ~4����r�~Χ��T+�Iv3��G�������8�f%;-������G���-����x^�Ԛy���儔x���`Ұ�̊A��Ws�j~�hI�R�ޜ��g3�Ύ��X�ݒ\�W<�La������c���Ž�[��U2~����>��y'3>��Q�h�.B����-~�9��珪xW�l�^�%��a�&��u����S���E��p�Ϻ���N�V�K\����9,����9+�Jn'�V�iv:��Z2�=��d�˶:�����=�𚝱,b�M4pu����Y���ty?���k����q,��̂z���珢�p���O���]4:��.g�Kݔ�	��h�6
eD@~�b>]�n������c����s�\΋��e�s)3+g3R��P�j`g��x2˥�Y,�iY�L>k�ڙY^��[�Ra&>T�$E�#��x��!�V~ ���N~6���R�|m^_�noY+�WK��y����x��A�ЪeR�L�WȎj��_�́z��n�sYv~v��Ga�����V~d�
��)���CE.��B=�*d��L^�
�L�-=�g3u�?'ma�P��˅\��O����9�]��f���[�y>�g�i������?�ӜvS������ɹ���sf�ڪ>X�l;+��Of�[[����Rf�A��J��d-Wjf%}S6��Q����#a����/�B%�$�QQv&D�IO�qG�^���}�i�'N<~S0+�t�rK��Dmt��{�6��ʓ4�[O_��e������^�A��<,9���҈�T�dR����t�^(�ݒs��q��+��޵z�e/�t�T�4%���S�T�����.�ۣ�Ҳx^����U3ݷnI��g�S�ɉ��t+�ef�L���jٛ�}��kd��B��x=oU����u��|8��̇�SF�Wc�L���LJ�� ��Er�^&��a����E}:z�A���j�Q�<���in�L��lL�}�ί �z�w���8l%�lO/��vS�J����s�{�܊B�X�9�Jq��^��k���ұ^j�o5[y4�!F)Zχ�e���%�Ӊa=6I\]ƣ��pE���N��N����_'K7�ŃV7���hQ,Ib}rU'V,'��D����r�&�zPzeܹ�f:���/������\�v�-	��Vt-W��c����V���Ē�Ѯ��ڲ|UK'��9wZJ��t�3~~���tr�W"�O˚��M��)���|�p�/���9֏k��y��g�T�Z��(Î�3]K�XL��7�a�#sAJ�l���l���l�Amn�Y�TuIw�{s��ʚ��n!���m~h6:�3�����3������Ft����!yjQ6�`�/��T�uS���a|�/�4G?i�;��❵�Rء�f���]���&�?(UB��1��^p4� (*J���^A�H�M{)� X�`��*�Í�n��4�8�G�� ޖ��{���������b5��E�1���_����6N�ik�����g���S��w:���S���ķ�?��?�O�?'}E�y�+�?b�ɫ]�'�1(~Z�?B���C�m�_�d^�F�f�>[r�݀�8�+��t} ʼ���h�%F����C�m�]�7�a�K�n��n�~e���W�&Ӿ�H��'���ϼ-r��te�t��?W|?���n
�������_�t��Hi-��c�ҟ���?��>��x����_����
]���B����'��[!'�������5��:�����7����/�8���������#Ƿ����a�7F����8~S�6~K��%��x�$�-���X�����~�������?FZ��#���ӧ������u��t�����QR����������>��GI���������1��=��������T����Q�)��O
%O�����������5���i��(i#���~�w@�#�O�O��GI�>����?�>����0�0����K]��������S��k�w�1���uü���~|Q�����-B!�7��?�x�j銽_ꚞ�R��?���7����?�t�d���N�?�C/���[�/r��+����Y������i-|D��?�#��$�c$����Ϸ�?E�����q[����Q�~���*��u<qz��(�]�����p*/k\��������s�b8���x�W�1���v��O��S�`��$�����m�W��r?�~	:�p{�������<刾�)�M\�����nr�g�������)k����,8
o^@���ܷQVu1���rd��g@��xh�%8��>�`��� ����]ΒUCYp&�E���܈�����Hwl��D��l���4f�{���D������G׀,oq#Y��M��%4����P��,��dd9
��ȲF8l�%ׇ�#D4 D�f��h�V�v��q�(�p��cX\y���}�mA{N�҇[�ޢ
d�
E��f	���;��q�ޙhB_��/��E֒��/<u��h&+
>�j�c�(� {���.g������nmؽ^}�,7�� �J���6�p��*r>p�N��Ud�ˊ��E��u�ay<k�W���%�\��q(��2p���/@A@hi?��l�������_F��7-�C]�&+��ʭ���I���P��`[�~Sp���az���r�z��X8^�8K��=�d`#^T5C������<P�>��Á;��n�q�bYi'�m
���DPsl2��NR�?��ȩ�� �'n8���8�!̓'(�6kt@� �A�q"OT���F� $@�(hR@�6הE�б۶����(m��Q}�L䏸�K[©��-���ͣ�{��ć���@�y}p�V�c	�k�`�飆堧	��;��V�<��;u����u)98HQ�>�H�\A��1uÔy�ЦX���с6�MޢS�F�nuw8J�~��71��o-��̵Z�Wn�.㋟�$M��&Р+/���\�Rk��K:��
����(�6���n�8���ܪq��=��!,X���L�1 �#�q?����P�*�!��B<F�5ʁWc<q�L]^�/�o�nb/7�J����w��T|��&��0��oǌ�5�S��o�N�w���-��q ����k�iG�)���{��`
w��k�f��#Ü��PE�9:�!C�˄�:�u�Uu^�Q2�9�%-�A_+�C��+�m�X+��z$��P]��"��SpV(�/*H^�i}mEЂ�@\��a�Թq�cq�� �V���t�/�x|P����j9� cӮR�P�Wg
sr�ڼn ����2�*t�^ ���o��j�/TeXk E�a��A��ax��9�|b�!�P��B��!/h!Xh��:"�p4 ��x�l��V���6��e���
0;F@��D(m����ah.��&��c��V&܆�E(��������l��(8��PI.W� ��r-��,_�7B��C0��$��D��U�KD�ĳU`� ��/0��BX�.����u�hdb��D�u�0q^W	Y�b�n������>� i:B����E�S��P
e��10;��+<k�
�N�0쐡��T�ʸ���ƌ����K,�,0C�ɜ>)�_:�D0������M�����dǅ�˃��,������ �^3�N��VYU�f�h�SfN��%�����e0�	 !���m�����/�1QdUfZ�5�gJM2��F�ac���.����`���ZHeÈʚ�8"s�y _���
ci�А�͔Xg`ٲ��.��<�������_ 
a�-x@o�m���|�6W�ј��|��/�����@}h*�ӶA��a����.
&�]!b"���* "���F�,,���d��e�S
�v���}@�)s j���B%�Kl�7Ѐ:L%aۣ50h'�eC�;�2"<.6�<P�@�!�#�K�so���ta�� �&�q�L�������=�]��;:���@A��6��ecݰQ��-�G;>ֽl���o�-��RYH0����xV(�3��!Rp�"���+�3;gf�t��*����ӷ dV:�^�s�n	v�0�q�/�+��������|k��(�!��)�g������A���-���o���F�mD0b"n��*=���lx���Z������F7�eU�ϣ�u�iQX��Ӓշ�r˅�jo��.���Z�.��d^q��Ӆ��L0�	愉�P��D��]���R�2Xx�:s 3���M�l��'*Pj��c-���(J	\���T�������y�����*X���!�6<M2�D��i��fg���z�N?��>��c��^��`8dZR�FfdN�*�B�����+�����ϑ��q|%�ۇ��VtE��ݷ���'���m.�%rEgO�+�gJ��yE�m����t[��Y,�-�O�D��̐�N��W�����b����Q!�D�5fT�XqEH+<�Є��gw������aK��M�g�S�}����u�t4wS�q�	wn�[��;(��ձl,�[�PLz�̌��Dj"�N҃�L����O�EKH�=���f�I�w�����j�+��ZW��*����r7>%]a*C�	t�U0	n�CC�H�[*�A�\�C�]d5܎��/�!���
*�Y�&*=tT�42����6�~�����:��]*��L��7q6m�K��*(��m mo~��!�}瞾�OM3Z*�@@��J(�f]
��]6���ᗪ����vO�hzZ��CQ7g�	]�E�K�gέ��K������ �i�(=����n�͉�8 =���4��Mf�
ؚr�Fpm�ĪF7�;��wp�uW(2���˼>�������q�	�ۊ
�dDe�U�b?�,\T��6�Q�Ё�rl�B5.jz`�P���%/�%����
��	F��[I�\�$܌,,��(UL�':�v&o��tx���� ꥬ{�B��n�{�9F����]h�
��K�]����7�&�m��s�d�f������ҟ��ѧM�m�l��.��}��~y�7d��������Ĺ4/��|{�<뺊_,a��W�z�d�����b���������]ys�J���O�po�����m�����2�K1/H @��51�}*��t�㽙�y��D􁎪����_fe&��:�7����R�'jF��h�"^�A!����Ą�Jms$�{��M�E���-I#����&^���_�q{ �⯏u����bm�Y�n?Z7��������L"��\3�g���]-�S��?��{���2 ��R�L���4Ɛb���N�p�M�Oy\��av,�Z��t��0�����(@���#?eYAݗ�@3:�)2��qBm ����N�v�<���օ����`B��Zڑ6M�a��E�`��_ 2��s/�x��]�w<�����I+����cu��8��/��Q��k��z��H;ڸ �'y<%B�Q�"��x�����pv��C���o��y�HO[���|&��`��0ֱ���Nƈ�Ɩ���+/�-���P�T� P1dO�7��{��p�M���gx�5�s�gD�wc��k�r`�16�K*O���K�� 4��=}c��$��I'9;�	!`a�)�l,5z-Lb؁J�<����H��_Yp��y�Aj�lob�Kgt��];��\}��[m�8�kA�}���G��1�1��Ӡ`��VT�D ��4�Ej�<L(@{�&��	�t��c�� ��Σ�M�ChH8,�*/�!L���Qs	��2��>ա�A��=��L��Ȭ٦>'�.j�����,�c�Q�aR�*0�"˞�wSC��@�0�I0�i�T���40������Ҽ���a���tk������%�!𚨎5ϙ �<K(A����bitEE�$Q�>$;����&L|�I�����hAJG������^�DB��]�	���Gk�a�`�5�S�ӎS����N�Cŀ[��`����䜥�Tq���1�n��B��<j�Yp�ǪD
G|�&vq-\�ce
��#�����M�c<��� �c��,�HO�; ?��j!W�L�Q�
��	��>�4dA������Ց�^DO\PQ�R�7~�V�/B:�.���z�W+�(h�)�I�6x�d§d�0b~��I���s���:�B��Z�5��1�c��s 6��_�H<�(�4r�'�J�)�7XJ�G���6q�P�V��BSg��X�����AE#��w�%l����IJ�&p��d���K�(� <-6���er��h\� ���È-[��㌌kO≾~O}�ߑ� S�Q�d).1c�I��L�P��g#W1]���z`b�t4r�j�샖��$|ˍf��|úbs��l��	1����s;�ƤZȬ����_�Jyw���U5�;-gAUr�5d�@��Ի���,�$̝�Æb� �/o�B����a;A�0P���l%$Ú� ��XBK�r�����cBQH<�=M������d 0¶�7�(���c�p��f@jc�ɳ\1�1}�rO��Ё���yN�x�}�����ܮQ�\M� .��-��(�mt�l�����d*t�����7y?ި�H�cJ��A6U����4�xYZ �!M��������7|��}�P��y�u)|�_�/�������O�:�o��p���$��;��E��}�t�TY��U|����p��f�D�<]�B!�
Cg<W���2��������ډ��\@	=t>��z�ش����_>�*� �S���{MB-�k�x֌��N��$V�~<_l��B�\soz4��}h�觓��-��cŎC�B�/y��f�s�U�C#qN�\B��$�'�p��m\J'�0�4YڥSG�%�����`�#߮���F�&�=Z8��p=�OE�������e}����^�6 ��Ts��ĸ����PÐ-]�/�@"z��nP�i/��g�f4�X�|�Q6��<�l�g�:���O&ǈƥf���q8�<�l3S�=~�{��m=4G!��<|dв�g=��M�ds3�(y��JS�a|q������!�$c	6�LDI��'�lc"�%+˄�v!���%ҿ�t�\�&\�T(�|�{��έ�~���17с�1�������{մ���=�4���G �:�1f�$gI%���gsE1��5�=`�,�U� �޴��!��f���!ty{���X5�6���za<�5�tdX�E�G�x�+����Y�����)�����D��0� �'�B��q�Qw'�!l��be=�t<Ř�G�x3��b����L��y���7/��C����Ό��l� t��P����r��	1�ͅ��T�R� �-��V�9Wgѩ��4���!

y�R�J4ÿom�O��KY���Kp�S�K�G�BiB�[.M�U����Z�����y�W��2n6��s��o�E��EHN��sL��DM��~��;/c��0]Ƕ�D��_z4�����Q�Η�T#�j���� rd\G�)�f8���Bkh�_��e��P�Q���G��\ԯ�*����Ϭ�(J$���;����#ɏ������V����~�����i74Ԗ��-Y�x( +w���;�z�5T+�RK������;�jd�]O�;J�sx�j�Q@��ֹ�,��J����(���/�#%N ��V�=���%����!�8�[��_��i��3�J$�F����/�.��@`�ճbЏG������oZ�����z(��D��Ҽ�f��m:����xX=���q�=u�{����4� �)���8k vґ+'��i}�Pb�i�M#Ĉ�<75:q�ӻ�q>�Ϧ��|���=�6"�ᬡ|$�e�^���B�FuZ`����_x�;d;����+A ��1�v�pg��cqs:���!
ڝp��y ��oqP��-��8���w���\]\�%v#����8r���@�n���<�=����o<p��]���o�7�0G�s�]�tR��@6�`��!4��h�L�p�����N���w�9�LI#kM�l�������L�������a� -���n'�OY ��J�ZS�C��,�Dz!�a�k!��w���t^��ytY�9A�Ubc'��nx��F]�?�o��e�����^!%���=v�N!6Zq�|I��v�J�MMa�.�G�� B{�pt��s\*���h����Y�)�Y��P��Jµ�W�)�c����:��&��Y�b���о�����8��-
d�D�C���<p"c��Lc�l7ﴘ�Ț��i\�=����GVsL;�l�y{0���9?��q	q]x���Bz��x<� �0,$;���8��^�#��k�N`/����dz}~y�'ˉX���Ðh�<�9��<�B[`�s��)��EÜ~U	*�F��ړ̥�7e��P�� ~��f�I`���±����\�E�qy�1֦���P�'����.v&NZ�P|̠�PBp�ں�%��i�	`x�lDPQ����'^T�����)x�G�Y��dԅC�飉 2�gǾ>�'\��r���S�k����6:/Yl���fZҨ�Zu�L� b�3�ƠfU5>��;��V�ÑXa��.���t(J5��a�Bg��0�H����&���:�`�{�I�q'�� ِa���UB\�=��m1��Ah��ZX�7���6��ސ���ٙ�gm�{�Z_�;���:��c��`B���IkQFo��)��H��ͩ�·1I`	�7�q>i�������IC��� �Q$�MR�>lU&os;���L扆�&$/�K��h`; %���a�U2)�It.Z:h���XV;|�6ҧUq��,A�Z8B���>{�UE�M�*��D5+��5s�ȕV1���b6�
?��Kc)�7��Gg@ �r�̠�~X"=�|<Ȯ��x���ّ�K�	��r����@?�/�b�,����'��!3�w�0��tp��R���`G�0s�� L�/��8���H�}lZ��"Ũ�b�gw�»1����_�����7���?����P��g���'��;�o���������Q������l�g���������Ï='�xW�����'�֐�B���u�"jx5��ru|�T�u���$.��S�5
�����V4������˕ؖ����l��YI�yu�*�RJ_��z�fe"[�{l5zR�?���e_������g����xP{N)��s�5��ji5�g���Y[���J�+�uNY��\���g �Sxq%��YJR���(Y,j�"f�����B�W#<xŢ,��{h�a`���ޭ���S���\�F��{��)��f���V�����M[�)c<��fe�o/%?�u��h��U�m�k����Fǧ�nc4߮;ۺ�<4��J�X�fR��ҹ��c_��&b�݋��-?W�uE6��YS�-���e����-�[���|o<m����݋�̱���]�l�������p'rqX_V6�F�jf�ʇ@9͎��n�{�U�p8I�Ti]�י�*�:������k�'�>���\�����leޙ��ӭ�5���F}i�O��\*��Ĥi�s�[kxo)���:�zP���~x+k�+�E\(�(6V�����ּ���@rSDD�Ң=|쓇���4�b�1�HY��7�$���c5�����Uw`l���KMNb�ȸ+-j5q,��L�y+�b�"����v���K=w�*��j�wz-k�c�P�����e��ٗb���}�T+��h��'[3��]��7�өӚ��v���Զ�`���B�p?�w�^���_.M�I���Z�9h�%s��:�z�~U�7��Jd�Km�n�.����z۩/�W�{���� ���őd��3I���|&CVS�z�/KO�:ӭ�^�UK����E�Yy٪��~s�o+�U�;>����E���6���N=�w6;u�e3�EcZJU7]����:Ӳ�ݞT��X�Q�(��,;m��ו�S~8�fgP�����p)6;���_v�݉o/���*�Z��Vq���������O����}���_��͝���\����!�����^�R��L����"�tg�[i�
/"��������	H����1@{ ��"ۮ>��x-^\I3���+�qu��5j�����W�{l�")���'�M��j�ZU��~����@�.73�����ItR�=l��s�v���gz]Q��DC� �#�����[-3�I�ic�����l�a֕�]������xu��������\7�ѳF���4���Z�����'b�W�F~�)������d2����h����,�f١�:��ݡW
���T銓����k��Ϧ�~��NY�w��k���[{��i����-�Mʚ+��\S��M�j]��+�\e�YɒXW/)Mo��}�t8�j�Z�x��O�V�8�N})I�u����Ͻ�>�V�$���3᜔m9wM75v��}ם�9Km���H��K޽��+�,���5�I��Ϯ<z-����ثl�l��捅��;�Tr�&�q�b$��ZmT�F��ƛ���ic���Jm���W�!ղۥ��_�� Dw�ܞ�Q����֨��U�{E-�k�����lY��tL�	��֨��3��=��R��7bR��v$i\���;�+���ưWz6���Z�R�cEW$�9مǊ��uyt�m�]�X��jJ�}�~�솶"?����k��z������U��+J�+�Ct/�zc
���r�0�0%�����N�:C����V����,oۋ��I� �.�(��(�`.�ʳ˭:%��$+��u7�g���i��k��r5}��Z�3F:g�y� ���f2yi*�n���q�Cp�Rז��x���D㞲<��8��KzV�_duF�&b*#@{�-^A�)d��H[/=x!�mRaaj��`�r-��>��p悤s��B��&�-��qD��Zy��HO���[QKM�L'����WQQt�V��wd��r�?:6��o��w�]�eԝ4j���v��^�p;Wr��.��t�x=�M}=����ýk�Yg�7��I��6ЁV�)7N"�H�T�H�'3X�<���S%z9���^x�P��[k� vE�-�MQZ�F�Mr;҉�(y:�ߎ�cL�5�D�=9a�v7��(����Y�j;�z��VP���q���xCi$LcE��ԶFV�3�lp��=0�w�1I��R)��{�/Ѷ��#�����aOj�k�2�dˣ���郷e�y$D43{�dD���ʚ��O�˖+Eb�R%8 ���Sny��&���@�aÊ���45�H�_z�$p�GS#EB7�`��;<���_rg���م:�dGL�%*�Y"������u�ea��*xRv�@���$C�0�)�C	�D�ٲ�����ޯx�8�4�8��+$�dSi�[�vQm���Q��';aH�F�/���⿇vҿ6��(����7��+����7�%�T}mN��$��k��؏o�s�;�.$h]wV_n�ˬB*a�
��O�]��M�Ut�Gt~���.�8�j!+&*O��h>)Kc�ɉ�c�s_8Y7,����̲hֈ���gӹ�xGo����q�/�\�H����(0^����Xb.�!\���=1�VQ7 �T��r��VK�f��E+�b ��l38�ˍ����?]zVj��I��Ʒ"�h�������9+�2�'����c-�1e�`�aZC��=&i��#W<U8V�R�h�l�%��8���hOO�,�U���ݞ���kk��H��ݬh�UrY��L,�Q	�QG�άl����|*�x�_�z��|\xXm�:!,�i���5i���F�Q�sR��j�]�9Qm��Y׳B_�F�O���ښr�\�i\�����m�.��Hi���@;)*���z]��|����s���t�PR��G�P����3g�[u��4&�VΤ�ձ�Y��D��r#n\Y����(C�a��b*=�{�@x�E�	\!�;����e�]��\jC�[	�%����"��P֩�w2�AmrT	X,A�ړG�ju@�EF�)��B=�a2r^p�v�[z4#�u�DBE�X�*��q��[��4���@1��m�tN��E�x��Q}�������i r��H;od?u�A��]7�)$���oW�x�_�&�o����f~k���@�C��2����������|_�}B�i��n��o{'t�8�k��#rI$�
X��9�]�����Y|��L4���p��K�ZIHf�)
Ys\�N��� �6T�E4G&�9aɷ�4��ԃ��n3!�,OM&��Cn�PX`��ći�Y &.��]�O�-b-�an[�c��Q�R96kh{��P��t��5�fd��2\eP7���^�������?I[BG,w�56��.zR��������e�0j��lG�Ǽ,����y<���ȅjf!�����K��*���i���3v&�2;`:��Uk?G��1[ۄ��4v�����:Q~�� $�2��q/j�Pg`GUf�6��.d��t����LY�֗�{6�$�^�O*{\����d����9O�@�VU��Tĕ�<�%ȊH�Q9��l��aez���i��R<�n�¿��=�uzai 1�ZA8 �g�\��$	ԝ���\Mh��wp?�H����.#p�K�upRe"l,�f�y�	��o̬�*{+_��セJZ	�3o둀k\�P�K�%*��T�
�'�k�wש���/���S��)����A��.���cן����X����w������u���L��6�c���]����o�Żˀ/*�����F������]+�?��j�u���׍A�O_ௐ��q�T��������U6�*��w��{�m��9��
��Z��3�}q���������K׿����u�̧lݾ!����K���rE��)IP~�҄{��X�����}�{�A�-�E1P�C����]Z�;�M�����/x������*M��|�m�O9���}�q����G�����k�W������Z�[:ߓ�~@⣳/��{�������MoIzY4�y�^K|L���^`���C;?'�+��H�ë��_��Q�ݑ1tG}L�W�Y���7�O�S��;nn�)��?�?��}V&����pC>�a����i:~Sӗ����n��Ώ��y��CWş�z_�y��!�v=y�������%��,=f����k��4���z��x������G���j�w�����q�����w�u��ޖu�&Y���ӥnB�M��V�e���ҽ�Q���G��t,�+�J��W�?aN�  